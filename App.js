/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React from 'react';
import {Provider as PaperProvider} from 'react-native-paper';

import {useDispatch} from 'react-redux';
import ReturnStoreAndPersistor from './root/store/store';
import {Provider} from 'react-redux';
// import 'react-native-gesture-handler';
import Routes from './root/components/router/routes';
import {PersistGate} from 'redux-persist/integration/react';

const App: () => React$Node = () => {
    const store = ReturnStoreAndPersistor();
    return (
        <>
            <Provider store={store.configureStore}>
                {/*<PersistGate loading={false} persistor={store.persistStore1}>*/}
                    <PaperProvider>
                        <Routes/>
                    </PaperProvider>
                {/*</PersistGate>*/}
            </Provider>
        </>
    );
};

export default App;
