
const initialState = {
    // loggedin: false,
    // isSignout: false,
    isLoading_system: false,
    fetching_data: false,
    // user: {
    //     username: 'Aubri.n'
    // }
}

const usersReducer = (state = initialState, action) => {
    switch (action.type) {
        case 'PURGE':
            return {...initialState};
        case 'FETCH_USERS':
            return {
                ...state,
                allUsers: action.payload
            };
        case 'FETCH_USER_DATA':
            state.isLoading_system = true
            state.fetching_data = true
            return {...state}
        case 'UPDATE_USER':
            return {
                ...state,
                currentUser: action.payload
            };
        default:
            return state;
    }
};

export default usersReducer
