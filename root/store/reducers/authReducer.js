import jwt_decode from "jwt-decode";

const initialState = {
    loggedin: false,
    is_fetching: false,
    isLoading_system: false,
    user: {
        bear: undefined,
    },
    user_creation:  {
        is_fetching: false,
        message: '',
        error: false,
        created: false
    }
}

const usersReducer = (state = initialState, action) => {
    switch (action.type) {
        case 'PURGE':
            return {...initialState}

        case 'CREATE_USER_RESET':
            state.user_creation.is_fetching = false
            state.user_creation.message = '';
            state.user_creation.created = false;
            state.user_creation.error = false;
            return {...state}
        case 'CREATE_USER_LOAD':
            state.user_creation.is_fetching = true;
            state.user_creation.message = 'Loading';
            return {...state}
        case 'CREATE_USER_SUCCES':
            state.user_creation.is_fetching = false
            state.user_creation.message = '';
            state.user_creation.created = true;
            state.user_creation.error = false;
            return {...state}
        case 'CREATE_USER_ERROR':
            state.user_creation.is_fetching = false
            state.user_creation.error = true
            state.user_creation.message = action.payload
            return {...state}
        case 'DEV_LOGOUT':
            state.loggedin = false;

            return {...state};
        case 'LOAD_USER_REQUEST':
            console.log(state)
            state.isLoading_system = true;
            state.is_fetching = true;
            return {...state};
        case 'SUCCES_USER_REQUEST':
            console.log(state)
            state.loggedin = true;
            state.is_fetching = true;
            state.isLoading_system = false;
            state.user.bear = action.payload.bear;

            var decoded = jwt_decode(action.payload.bear);
            console.log('---------------------')
            console.log(decoded);
            console.log('----------');

            state.user.data = decoded;



            return {...state};
        default:
            return state;
    }
};

export default usersReducer
