const initialState = {
    global_loading: false,
    user_data: {
        user_sessions: {
            fetching: false,
            is_fetched: false,
            last_sync: undefined,
            data: [],
        },
        user_spots: {
            fetching: false,
            is_fetched: false,
            last_sync: undefined,
            data: [],
        },
        user_gear: {
            fetching: false,
            is_fetched: false,
            last_sync: undefined,
            data: [],
        },
        sports: {
            fetching: false,
            is_fetched: false,
            last_sync: undefined,
            data: [],
        },
        user_graphs: {
            sessions: {
                fetching: false,
                is_fetched: false,
                last_sync: undefined,
                data: [],
            },
            sport: {
                fetching: false,
                is_fetched: false,
                last_sync: undefined,
                data: [],
            },
            sessions_count: {
                fetching: false,
                is_fetched: false,
                last_sync: undefined,
                data: [],
            },
        },
    },
};

const usersReducer = (state = initialState, action) => {
    switch (action.type) {
        case 'PURGE':
            return {...initialState};
        case 'FETCH_USER_SPORT_SUCCES':
            state.user_data.sports.fetching = false;
            state.user_data.sports.is_fetched = true;
            state.user_data.sports.data = action.payload;
            return {...state};
        case 'FETCH_USER_SPORT':
            state.user_data.sports.fetching = true;
            state.user_data.sports.is_fetched = false
            return {...state};
        case 'FETCH_USER_SPORT_ERROR':
            state.user_data.sports.fetching = false;
            state.user_data.sports.is_fetched = false;
            return {...state};

        case 'FETCH_SPOTS_SUCCES':
            state.user_data.user_spots.fetching = false;
            state.user_data.user_spots.is_fetched = true;
            return {...state};
        case 'FETCH_SESSIONS_REST':
            state.user_data.user_sessions.fetching = false;
            state.user_data.user_sessions.is_fetched = false;
            return {...state};
        case 'FETCH_SESSIONS_SUCCES':
            state.user_data.user_sessions.fetching = false;
            state.user_data.user_sessions.is_fetched = true;
            state.user_data.user_sessions.data = action.payload;
            return {...state};
        case 'FETCH_USER_GEAR_SUCCES':
            state.user_data.user_gear.fetching = false;
            state.user_data.user_gear.is_fetched = true;
            state.user_data.user_gear.data = action.payload;
            return {...state};
        case 'FETCH_USER_GEAR':
            state.user_data.user_gear.fetching = true;
            state.user_data.user_gear.is_fetched = false
            return {...state};
        case 'FETCH_USER_GEAR_ERROR':
            state.user_data.user_gear.fetching = false;
            state.user_data.user_gear.is_fetched = false;
            return {...state};

        case 'FETCH_SPOTS':
            return {...state};
        case 'FETCH_SESSIONS':
            state.user_data.user_sessions.fetching = true;
            state.user_data.user_sessions.is_fetched = false;
            return {
                ...state
            };
        case 'FETCH_SESSIONS_ERROR':
            state.user_data.user_sessions.fetching = false;
            state.user_data.user_sessions.is_fetched = true;
            return {
                ...state
            };
        case 'UPDATE_USER':
            return {
                ...state,
                currentUser: action.payload,
            };
        case 'FETCH_GRAPH':
            console.log('in reducer');
            state.user_data.user_graphs[action.payload.graph].fetching = true;
            state.user_data.user_graphs[action.payload.graph].is_fetched = false;
            return {
                ...state,
            };

        case 'FETCH_GRAPH_SUCCES':
            state.user_data.user_graphs[action.payload.graph].fetching = false;
            state.user_data.user_graphs[action.payload.graph].is_fetched = true;
            state.user_data.user_graphs[action.payload.graph].last_sync = new Date().getTime() / 1000;

            state.user_data.user_graphs[action.payload.graph].data = action.payload.data.data;
            return {
                ...state,
            };
        case 'FETCH_USER_SYSTEM_DATA':
            state.user_data[action.payload.data_type].fetching = true;
            state.user_data[action.payload.data_type].is_fetched = false;
            return {
                ...state,
            };
        case 'FETCH_USER_SYSTEM_DATA_ERROR':
            state.user_data[action.payload.data_type].fetching = false;
            state.user_data[action.payload.data_type].is_fetched = true;
            return {
                ...state,
            };
        case 'FETCH_USER_SYSTEM_DATA_SUCCES':
            state.user_data[action.payload.data_type].fetching = false;
            state.user_data[action.payload.data_type].is_fetched = true;
            state.user_data[action.payload.data_type].last_sync = new Date().getTime() / 1000;
            state.user_data[action.payload.data_type].data = action.payload.data;
            return {
                ...state,
            };
        case 'POST_SESSION':


            return {
                ...state,
            };
        case 'RELOAD_USER_SPOTS':
            state.user_data.user_spots.is_fetched = false;
            state.user_data.user_spots.fetching = false;

            return {
                ...state,
            };
        case 'POST_SESSION_SUCCES':
            return {
                ...state,
            };
        default:
            return {...state};
    }
};

export default usersReducer;
