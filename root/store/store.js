
import { createStore, combineReducers, applyMiddleware } from 'redux';
import thunk from 'redux-thunk'; //for asynchronous actions
import { composeWithDevTools } from 'redux-devtools-extension'
import { createLogger } from 'redux-logger'
/* Import all reducers */
import SystemReducer from './reducers/systemReducer';
import AuthReducer from './reducers/authReducer';
import UserReducer from './reducers/userReducer';
import AsyncStorage from '@react-native-community/async-storage';

import { persistStore, persistReducer } from 'redux-persist'

const persistConfig = {
    key: 'root',
    storage: AsyncStorage
}

const rootReducer = combineReducers({
    systemReducer: SystemReducer,
    authReducer: AuthReducer,
    userReducer: UserReducer
})
const middlewares = [];

const logger = createLogger();

middlewares.push(logger);
middlewares.push(thunk);

const persistedReducer = persistReducer(persistConfig, rootReducer)

const configureStore = composeWithDevTools(applyMiddleware(...middlewares))(createStore)(persistedReducer);

// const configureStore = createStore(rootReducer, composeWithDevTools(
//     applyMiddleware(thunk,logger)
// ))

export default () => {
    let persistStore1 = persistStore(configureStore);
    return { configureStore, persistStore1 }
}
//


// export default () => {
//     let configureStore = createStore(persistedReducer)
//     let persistor = persistStore(configureStore)
//     return { configureStore, persistor }
// }


// export default configureStore;
