import axios from 'axios';
import * as systemActions from './systemActions';

const localhost = 'http://192.168.0.125/surfspot_backend/public';
const LIVE_SERVER = 'https://surfspot.auno.be/public';
const SERVER = LIVE_SERVER;

export function fetchUserSessions(page) {

    return function (dispatch, getState) {

        dispatch({
            type: 'FETCH_SESSIONS',
            payload: true,
        });

        axios({
            // headers: {'Content-Type':'application/json'},
            method: 'GET',
            url: SERVER + `/api/sessions?order[seesion_start]=desc&page=${page}&user=${getState().authReducer.user.data.id}`,
        }).then(function (response) {
            let objectArray = [];
            console.log('fetched');

            response.data.map((item)=>{
                objectArray.push(item);
            })

            dispatch({
                type: 'FETCH_SESSIONS_SUCCES',
                payload: objectArray,
            });

        }).catch(function (error) {

            dispatch({
                type: 'FETCH_SESSIONS_ERROR',
                payload: true,
            });

            console.log(error.response.data);
        });
    };
}

export function fetchUserGear() {

    return function (dispatch, getState) {

        dispatch({
            type: 'FETCH_USER_GEAR',
            payload: true,
        });

        axios({
            // headers: {'Content-Type':'application/json'},
            method: 'GET',
            url: SERVER + `/api/gears?user=${getState().authReducer.user.data.id}`,
        }).then(function (response) {

            let objectArray = [];

            response.data.map((item)=>{
                console.log(item)
                // if(objectArray[])
                let res = item.sport.split("/");
                let sportId= res[res.length-1];
                if(objectArray[sportId] === undefined){
                    objectArray[sportId] = [];
                }
                if(objectArray[sportId][item.type] === undefined){
                    objectArray[sportId][item.type] = [];
                }
                objectArray[sportId][item.type].push(item);
            })
            console.log(objectArray);

            dispatch({
                type: 'FETCH_USER_GEAR_SUCCES',
                payload: objectArray,
            });

        }).catch(function (error) {
            dispatch({
                type: 'FETCH_USER_GEAR_ERROR',
                payload: true,
            });
            console.log(error.response.data);
        });
    };
}

export function fetchUserGearNormaLized() {

    return function (dispatch, getState) {

        dispatch({
            type: 'FETCH_USER_GEAR',
            payload: true,
        });

        axios({
            // headers: {'Content-Type':'application/json'},
            method: 'GET',
            url: SERVER + `/api/gears?user=${getState().authReducer.user.data.id}`,
        }).then(function (response) {

            dispatch({
                type: 'FETCH_USER_GEAR_SUCCES',
                payload: response.data,
            });

        }).catch(function (error) {
            dispatch({
                type: 'FETCH_USER_GEAR_ERROR',
                payload: true,
            });
            console.log(error.response.data);
        });
    };
}


export function createNewSpot(spotObject) {

    return function (dispatch, getState) {
        dispatch({
            type: 'POST_SPOT',
            payload: true,
        });

        axios({
            // headers: {'Content-Type':'application/json'},
            method: 'POST',
            url: SERVER + '/api/spots',
            data: spotObject,
        }).then(function (response) {
            console.log(response.data);
            dispatch(systemActions.fetchNearesSpots());
            dispatch({
                type: 'RELOAD_USER_SPOTS',
                payload: true,
            });


        }).catch(function (error) {
            console.log(error.response.data);
        });
    };
}


export function createNewSession(sessionObject) {

    return function (dispatch, getState) {
        // let state = getState();

        dispatch({
            type: 'POST_SESSION',
            payload: true,
        });

        axios({
            // headers: {'Content-Type':'application/json'},
            method: 'POST',
            url: SERVER + '/api/sessions',
            data: sessionObject,
        }).then(function (response) {

            console.log(response.data);
            console.log('cucces');

            dispatch({
                type: 'POST_SESSION_SUCCES',
                payload: true,
            });
            // dispatch(systemActions.fethUserSessionCount());
            // dispatch(systemActions.fethUserSessionsGraph());
            // dispatch(systemActions.fethUserSportGraph());

            // dispatch({
            //     type: 'FETCH_GRAPH_SUCCES',
            //     payload: {graph: 'sessions', data:response.data,}
            // });

        }).catch(function (error) {
            console.log(error.response.data);

            dispatch({
                type: 'POST_SESSION_ERROR',
                payload: true,
            });

            // dispatch({
            //     type: 'CREATE_USER_ERROR',
            //     payload: error.response.data.message
            // });
        });

    };
}

export async function addOrRemoveNewSpotToFavorites(removeOrAdd,favoData,user,spot) {
    if(!removeOrAdd){
        console.log('add');
        axios({
            // headers: {'Content-Type':'application/json'},
            method: 'POST',
            url: SERVER+'/api/favorite_user_spots',
            data: {
                userId: "/api/users/"+ user,
                spotId: "/api/spots/" + spot.id
            },
        }).then(function (response) {
           console.log(response);
        }).catch(function (error) {
            console.log(error.response.data);
        });
    }else{
        console.log('delete');

        axios({
            // headers: {'Content-Type':'application/json'},
            method: 'DELETE',
            url: SERVER+'/api/favorite_user_spots/'+favoData.id,
        }).then(function (response) {
            console.log('deleted');
        }).catch(function (error) {
            console.log('error on delete');
        });
    }
}

export function fetchSports() {

    return function (dispatch, getState) {

        dispatch({
            type: 'FETCH_USER_SPORT',
            payload: true,
        });

        axios({
            // headers: {'Content-Type':'application/json'},
            method: 'GET',
            url: SERVER + '/api/sports',
        }).then(function (response) {

            dispatch({
                type: 'FETCH_USER_SPORT_SUCCES',
                payload: response.data,
            });

        }).catch(function (error) {
            dispatch({
                type: 'FETCH_USER_SPORT_ERROR',
                payload: true,
            });
            console.log(error.response.data);
        });
    };
}

export function updateFavogear(gearList) {
    return function (dispatch, getState) {

        console.log(gearList);

        axios({
            // headers: {'Content-Type':'application/json'},
            method: 'POST',
            url: SERVER + '/gear/favorite/update',
            data: gearList
        }).then(function (response) {
            dispatch({
                type: 'USER_FAVO_GEAR_UPDATE',
                payload: gearList,
            });

        }).catch(function (error) {
            console.log(error.response.data);
        });
    }
}

export function addNewGear(gearItem,userId) {
    return function (dispatch, getState) {
        // brand: "Goya"
        // size: "84"
        // sportName: "Windsurf"
        // sport_id: 1
        // type: "Board"
        // type_value: "Thruster"
        axios({
            // headers: {'Content-Type':'application/json'},
            method: 'POST',
            url: SERVER + '/api/gears',
            data: {
                user: "api/users/"+userId,
                value: gearItem.size,
                type: gearItem.type,
                brand: gearItem.brand,
                sport: "api/sports/"+gearItem.sport_id,
                typeValue: gearItem.type_value,
                userDefault: false
            }
        }).then(function (response) {
            dispatch(fetchUserGearNormaLized());

        }).catch(function (error) {
            alert(error.response.data);
        });
    }
}


export function deleteUserGearById(id) {
    return function (dispatch, getState) {
        axios({
            headers: {'Content-Type':'application/json'},
            method: 'DELETE',
            url: SERVER + '/api/gears/'+id
        }).then(function (response) {
            // dispatch(userActions.fetchUserGear());
            dispatch(fetchUserGearNormaLized());

        }).catch(function (error) {
            console.log(error.response.data)
            alert('failed gear delete');
        });
    }
}






