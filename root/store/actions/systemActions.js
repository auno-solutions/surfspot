import axios from 'axios';
const localhost = 'http://192.168.0.125/surfspot_backend/public';
const LIVE_SERVER = 'https://surfspot.auno.be/public';

const SERVER = LIVE_SERVER;


export function fethUserData(props) {

    return function (dispatch, getState) {

        // axios({
        //     // headers: {'Content-Type':'application/json'},
        //     method: 'POST',
        //     url: SERVER+'/app/create/user',
        //     data: {
        //     },
        // }).then(function (response) {
        //     console.log(response.data.token);
        //
        //     // dispatch({
        //     //     type: 'CREATE_USER_SUCCES',
        //     //     payload: {bear:response.data.token, user: undefined},
        //     // });
        //
        // }).catch(function (error) {
        //     console.log(error.response.data)
        //     // dispatch({
        //     //     type: 'CREATE_USER_ERROR',
        //     //     payload: error.response.data.message
        //     // });
        // });

    };
}

export function fethUserSessionsGraph(props) {
    return function (dispatch, getState) {
        let state = getState();
        let userid = state.authReducer.user.data.id;


        // dispatch({
        //     type: 'FETCH_GRAPH',
        //     payload: {graph: 'sessions', payload: true}
        // });

        axios({
            // headers: {'Content-Type':'application/json'},
            method: 'GET',
            url: SERVER + '/api/sessions/' + userid + '/graph',
            data: {
                user_id: userid,
            },
        }).then(function (response) {
            dispatch({
                type: 'FETCH_GRAPH_SUCCES',
                payload: {graph: 'sessions', data: response.data},
            });

        }).catch(function (error) {
            console.log(error.response.data);
            // dispatch({
            //     type: 'CREATE_USER_ERROR',
            //     payload: error.response.data.message
            // });
        });

    };
}

export function fethUserSportGraph() {
    return function (dispatch, getState) {
        let state = getState();
        let userid = state.authReducer.user.data.id;

        // dispatch({
        //     type: 'FETCH_GRAPH',
        //     payload: {graph: 'sport', payload: true}
        // });

        axios({
            // headers: {'Content-Type':'application/json'},
            method: 'GET',
            url: SERVER + '/api/sport/' + userid + '/graph',
            data: {
                user_id: userid,
            },
        }).then(function (response) {
            dispatch({
                type: 'FETCH_GRAPH_SUCCES',
                payload: {graph: 'sport', data: response.data},
            });

        }).catch(function (error) {
            console.log(error.response.data);
            // dispatch({
            //     type: 'CREATE_USER_ERROR',
            //     payload: error.response.data.message
            // });
        });


    };
}

export function fethUserSessionCount() {
    return function (dispatch, getState) {
        let state = getState();
        let userid = state.authReducer.user.data.id;

        // dispatch({
        //     type: 'FETCH_GRAPH',
        //     payload: {graph: 'sessions_count', payload: true}
        // });

        axios({
            // headers: {'Content-Type':'application/json'},
            method: 'GET',
            url: SERVER + '/api/sessions/' + userid + '/count',
            data: {
                user_id: userid,
            },
        }).then(function (response) {

            console.log('---------------*****');

            dispatch({
                type: 'FETCH_GRAPH_SUCCES',
                payload: {graph: 'sessions_count', data: response.data},
            });

        }).catch(function (error) {
            console.log(error.response.data);
            // dispatch({
            //     type: 'CREATE_USER_ERROR',
            //     payload: error.response.data.message
            // });
        });


    };
}

export function fethUserSpots() {
    return function (dispatch, getState) {
        let state = getState();
        let userid = state.authReducer.user.data.id;

        console.log('in fethUserSpots');

        dispatch({
            type: 'FETCH_USER_SYSTEM_DATA',
            payload: {data_type: 'user_spots'},
        });

        axios({
            // headers: {'Content-Type':'application/json'},
            method: 'GET',
            url: SERVER + '/api/user/' + userid + '/spots',
            data: {
                user_id: 'userid',
            },
        }).then(function (response) {
            dispatch({
                type: 'FETCH_USER_SYSTEM_DATA_SUCCES',
                payload: {data_type: 'user_spots', data: response.data.set},
            });

        }).catch(function (error) {
            alert(error)
            dispatch({
                type: 'FETCH_USER_SYSTEM_DATA_ERROR',
                payload: {data_type: 'user_spots', data: response.data.set},
            });
            console.log(error.response.data);
        });


    };
}


export async function fetchNearesSpots(coord) {
    return await axios({
        headers: {'Content-Type': 'application/json'},
        method: 'POST',
        url: SERVER + '/api/spots/nearest',
        data: {
            user_id: 'userid',
            coord: coord,
        },
    }).then(function (response) {
        console.log(response.data.data)
        return response.data.data;

    }).catch(function (error) {
        console.log(error)
    });
}


export async function fetchSpotDetails(spot) {

    console.log(spot)

    return await axios({
        headers: {'Content-Type': 'application/json'},
        method: 'POST',
        url: SERVER + '/api/spots/detail',
        data: {
            spot: spot.id,
        },
    }).then(function (response) {
        return response.data.data;

    }).catch(function (error) {
        console.log(error.response)
    });
}

export async function fetchSpotStats(spotId) {

    return await axios({
        headers: {'Content-Type': 'application/json'},
        method: 'POST',
        url: SERVER + '/spot/leaderboard',
        data: {
            "id": spotId,
        },
    }).then(function (response) {

        console.log('fetch leader board')
        console.log(response.data.data);
        return response.data.data;

    }).catch(function (error) {
        console.log('fetch leader board error')

        console.log(error.response)

    });
}


export async function fethUserSpotsCount(user) {

    axios({
        // headers: {'Content-Type':'application/json'},
        method: 'GET',
        url: SERVER + '/api/favorite_user_spots?userId='+user,
        data: {
            userId: "/api/users/27",
            spotId: "/api/spots/2"
        },
    }).then(function (response) {
        console.log('fetched')
        return response.data.length;
    }).catch(function (error) {
        console.log('error in fetch count spots');
    });

}



export async function countSpotList(userid) {

    axios({
        // headers: {'Content-Type':'application/json'},
        method: 'GET',
        url: SERVER + '/api/user/' + userid + '/spots'
    }).then(function (response) {
        console.log(response.data.set.length)
    }).catch(function (error) {
        console.log(error.data);
    });


}

