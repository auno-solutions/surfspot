import axios from 'axios';


const localhost = 'http://192.168.0.125/surfspot_backend/public';
const LIVE_SERVER = 'https://surfspot.auno.be/public';

const SERVER = LIVE_SERVER;


export function createUser(props) {


    return function (dispatch, getState) {

        dispatch({
            type: 'CREATE_USER_LOAD',
            payload: true,
        });
        axios({
            // headers: {'Content-Type':'application/json'},
            method: 'POST',
            url: SERVER+'/app/create/user',
            data: {
                username: props.email.value,
                email: props.email.value,
                password: props.password.value,
            },
        }).then(function (response) {
            console.log(response.data.token);

            dispatch({
                type: 'CREATE_USER_SUCCES',
                payload: {bear:response.data.token, user: undefined},
            });

        }).catch(function (error) {
            console.log(error.response.data)
            dispatch({
                type: 'CREATE_USER_ERROR',
                payload: error.response.data.message
            });
        });

    }
}

export function fetchUser(props) {
    // Interpreted by the thunk middleware:
    return function (dispatch, getState) {
        let email = props.email;
        let password = props.password;
        let state = getState();

        dispatch({
            type: 'LOAD_USER_REQUEST',
            payload: true,
        });
        console.log('icxi');
        console.log(state.authReducer.loggedin);

        if (state.authReducer.loggedin) {
            console.log('return');
            return;
        }

        axios({
            // headers: {'Content-Type':'application/json'},
            method: 'POST',
            url: SERVER+'/api/login_check',
            data: {
                username: props.email.value,
                password: props.password.value,
            },
        }).then(function (response) {
            dispatch({
                type: 'SUCCES_USER_REQUEST',
                payload: {bear:response.data.token, user: undefined},
            });
            //
            // dispatch({
            //     type: 'FETCH_USER_DATA',
            //     payload: true,
            // });


        }).catch(function (error) {
            alert('inlog error')
            console.log('request error')
            dispatch({
                type: 'ERROR_USER_REQUEST',
                payload: error.response.data,
            });
        });


        // Dispatch vanilla actions asynchronously
        // fetch(`http://myapi.com/users/${userId}/posts`).then(
        //     response =>
        //         dispatch({
        //             type: 'LOAD_POSTS_SUCCESS',
        //             userId,
        //             response
        //         }),
        //     error =>
        //         dispatch({
        //             type: 'LOAD_POSTS_FAILURE',
        //             userId,
        //             error
        //         })
        // )
    };
}
