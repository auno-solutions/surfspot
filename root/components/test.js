



import { connect } from 'react-redux'
import { Text, View } from 'react-native';
import React, { useEffect } from 'react';

const HelloWorldApp = (props) => {


    useEffect(() => {
        props.dispatch({
            type: 'TEST',
            payload: 'test'
        });


    },[]);

    return (
        <View style={{
            flex: 1,
            justifyContent: 'center',
            alignItems: 'center'
        }}>
            <Text>Hello, world!</Text>
        </View>
    );
}

export default connect(store => {
    return{...store}
})(HelloWorldApp);


