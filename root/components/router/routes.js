import {connect, Provider} from 'react-redux';
import {Text, View} from 'react-native';
import React, {useEffect} from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';


import HomeScreen from '../screens/home/homeScreen';
import SpotTabs from '../screens/Spots/spotTabs';
import SessionScreen from '../screens/sessions/sessionScreen';
import ProfileScreen from '../screens/profile/profileScreen';

import SignInScreen from '../auth/login';
import RegisterScreen from '../auth/register';
import SplashScreen from '../screens/splash/splashScreen';
import Ionicons from 'react-native-vector-icons/Ionicons';

import addSessionScreen1 from '../screens/sessions/addSession';
import addSessionScreen from '../screens/sessions/newSession';
import spotDetailScreen from '../screens/Spots/detail/spotDetail'
import newSpotScreen from '../screens/Spots/newSpot'
import sportDetailScreen from '../screens/profile/gear/sportDetailList';
import addGearScreen from '../screens/profile/gear/addGear';

const Routes = (props) => {
    const Stack = createStackNavigator();



    useEffect(() => {
        console.log(props.authReducer.loggedin);
    }, []);


    if (props.systemReducer.isLoading_system === true && props.systemReducer.fetching_data === true) {
        return <SplashScreen/>;
    }

    const Tab = createBottomTabNavigator();


    function HomeTabs() {

        return (
            <Tab.Navigator
                screenOptions={({route}) => ({
                    tabBarIcon: ({focused, color, size}) => {
                        let iconName;

                        if (route.name === 'Home') {
                            iconName = focused
                                ? 'ios-home'
                                : 'ios-home-outline';
                        } else if (route.name === 'Spots') {
                            iconName = focused ? 'ios-help-buoy' : 'ios-help-buoy-outline';
                        } else if (route.name === 'Session') {
                            iconName = focused ? 'albums' : 'albums';
                        } else if (route.name === 'Profile') {
                            iconName = focused ? 'ios-man' : 'ios-man-outline';
                        }

                        // You can return any component that you like here!
                        return <Ionicons name={iconName} size={size} color={color}/>;
                    },
                })}
                tabBarOptions={{
                    activeTintColor: 'tomato',
                    inactiveTintColor: 'gray',
                }}
            >
                <Stack.Screen name="Home" component={HomeScreen}/>
                <Stack.Screen name="Spots" component={SpotTabs}/>
                <Stack.Screen name="Session" component={SessionScreen}/>
                <Stack.Screen name="Profile" component={ProfileScreen}/>
            </Tab.Navigator>);
    }


    return (
        <NavigationContainer>
            {props.authReducer.loggedin === false ?
                // No token found, user isn't signed in
                <Stack.Navigator>
                    <Stack.Screen
                        name="SignIn"
                        component={SignInScreen}
                        options={{
                            title: 'Sign in',
                            headerShown: false,
                            // When logging out, a pop animation feels intuitive
                            // You can remove this if you want the default 'push' animation
                            animationTypeForReplace: props.isSignout ? 'pop' : 'push',
                        }}
                    />
                    <Stack.Screen name="Register" component={RegisterScreen}/>
                </Stack.Navigator>
                :
                <Stack.Navigator>
                    <Stack.Screen name="HomeTabs"
                      options={{
                          headerShown: false,
                      }}
                      component={HomeTabs}/>
                    <Stack.Screen name="Spot detail" component={spotDetailScreen}/>
                    <Stack.Screen name="New Spot" component={newSpotScreen}/>
                    <Stack.Screen name="New Session" component={addSessionScreen}/>
                    <Stack.Screen name="Sport detail" component={sportDetailScreen}/>
                    <Stack.Screen name="New gear" component={addGearScreen}/>
                </Stack.Navigator>


            }

        </NavigationContainer>
    );
};

export default connect(store => {
    return {...store};
})(Routes);


