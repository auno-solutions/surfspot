import * as React from 'react';
import {View, StyleSheet, Text} from 'react-native';
import {Button, Card, Colors, IconButton, Snackbar, Title} from 'react-native-paper';
import {connect} from 'react-redux';
import {useEffect} from 'react';
import axios from 'axios';
import * as userActions from '../../store/actions/userActions';
import * as systemActions from '../../store/actions/systemActions';
import {useNavigation} from '@react-navigation/native';

const carouselSpotItem = (props) => {
    const ICON = 'star-outline';
    const ICON_FAVORITE= 'star';

    const [loaded, setLoaded] = React.useState(false);
    const [currentSpot, setCurrentSpot] = React.useState(props.spot);
    const [isFavorite, setIsFavorite] = React.useState(false);
    const [user, setUser] = React.useState(props.authReducer.user.data.id);
    const [favoData, setFavoData] = React.useState([]);
    const [iconImage, setIconImage] = React.useState('loading');

    const navigation = useNavigation();
    const LOCAL = 'http://192.168.0.125/surfspot_backend/public';
    const LIVE_SERVER = 'https://surfspot.auno.be/public';
    const SERVER = LIVE_SERVER

    async function fetchUserFavorites(){
        axios({
            headers: {'Content-Type':'application/json'},
            method: 'GET',
            url: SERVER +'/api/favorite_user_spots?userId='+user+'&spotId=' + currentSpot.id,
        }).then(function (response) {

            if(response.data.length > 0){
                setFavoData(response.data[0]);
                setIsFavorite(true);
            }
            setLoaded(true);
            getCorrectIcon();

        }).catch(function (error) {
            setLoaded(true);
            getCorrectIcon();
        });

    }

    useEffect(() => {
        if(!loaded){
            if(props.spot.user_id == user){
                setIsFavorite(true);
            }else{
                fetchUserFavorites()
            }
        }
        getCorrectIcon();
    });


    function updateSpotFavorite() {
        if(props.spot.user_id != user) {
            userActions.addOrRemoveNewSpotToFavorites(isFavorite, favoData, user, props.spot);
            setIsFavorite(!isFavorite);
            // props.dispatch(systemActions.fethUserSpots());

        }else {
            alert('You own this spot,you cannot remove this nop')
        }
    }

    function getCorrectIcon() {

        if(isFavorite){
            setIconImage(ICON_FAVORITE);
            return null;
        }
        if(!loaded){
            setIconImage('loading')
            return null;
        }
        setIconImage(ICON);
    }

    return (
        <View style={{maxHeight: 150}}>
            <Card key={'keyspot-' + currentSpot.id}
                  onPress={e => {
                      let r = {
                          latitude: parseFloat(currentSpot.lattitude),
                          longitude: parseFloat(currentSpot.longitude),
                          latitudeDelta: props.latitudeDelta,
                          longitudeDelta: props.longitudeDelta,
                      };
                      if(props.map != undefined || props.map != null){
                          props.map.current.animateToRegion(r, 1000);
                      }

                  }}>

                <Card.Content>

                    <View style={styles.cardHeading}>
                        <View style={styles.cardTitle}>
                            <IconButton
                                style={styles.headerIcons}
                                icon="crosshairs-gps"
                                size={20}
                                color={Colors.blue200}

                            />
                            <Title>{currentSpot.name}</Title>
                        </View>
                        <View>
                            <IconButton
                                style={styles.headerIcons}
                                icon={iconImage}
                                color={Colors.red500}
                                size={20}
                                onPress={() => updateSpotFavorite()}
                            />
                        </View>
                    </View>

                    <Card.Title style={styles.cardSubTitle} title=""
                                subtitle={Math.round(currentSpot.distance * 100) / 100 + 'km from your location'}/>

                    <View style={styles.cardButtonBox}>
                        <Button mode="contained" onPress={() => {
                            navigation.navigate('Spot detail', {spot: props.spot});
                        }}>
                            Details
                        </Button>
                        <Button mode="contained" onPress={() =>{
                            navigation.navigate('New Session', {spot: props.spot});
                        }}>
                            add session
                        </Button>
                    </View>
                </Card.Content>
                {/*<Card.Cover source={{uri: 'https://picsum.photos/700'}}/>*/}
            </Card>
        </View>
        );
};

const styles = StyleSheet.create({

    cardButtonBox: {
        flexDirection: 'row',
        justifyContent: 'space-evenly',
    },
    cardSubTitle: {
        marginLeft: -3,
        marginTop: -35,
        marginBottom: -20,
    },
    cardHeading: {
        marginTop: -10,
        flexDirection: 'row',
        justifyContent: 'space-between',
    },
    cardTitle: {
        flexDirection: 'row',
    },
    headerIcons: {
        marginTop:3
    }
});

export default connect(store => {
    return {...store};
})(carouselSpotItem);

