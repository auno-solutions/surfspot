



import { connect } from 'react-redux'
import {Image, StatusBar, StyleSheet, Text, View} from 'react-native';
import React, {useEffect, useState} from 'react';

import {Avatar, Button, Card, Title, Paragraph, Caption, IconButton, Colors} from 'react-native-paper';
import {black} from 'react-native-paper/src/styles/colors';
import Moment from 'moment';


const Sessionitem = (props) => {
    const [session, setSession] = useState(props.parent.item);




    useEffect(() => {
        console.log(session.user.username)
        // console.log(props.parent.item.description);
    });


    async function fetchUserDetals(){
        // /surfspot_backend/public/api/users/26
    }


    function getsportIcon(sportName) {
        switch (sportName) {
            case 'surf_color.png':
                return require('../../assets/sport/surf_color.png');
            case 'kitesurf_color.png':
                return require('../../assets/sport/kitesurf_color.png');
            case 'windsurfing_color.png':
                return require('../../assets/sport/windsurfing_color.png');
            default:
                return require('../../assets/sport/surf_color.png');
        }

    }

    const LeftContent = props => <Avatar.Image {...props} size={50} source={getsportIcon(session.sport.sportIcon)} />
    const LeftContentImage = props => <Image style={styles.tinyLogo} source={getsportIcon(session.sport.sportIcon)}/>
    const rightIcon = props => <IconButton {...props} icon="information" color={Colors.blue500}size={25}onPress={() => console.log('Pressed')}/>

    return (
        <View style={styles.item}>
            <Card>
                {/*<Card.Cover source={{ uri: 'https://picsum.photos/700' }} />*/}
                <Card.Title title={session.sport.sportName} subtitle={session.user.username +' @'+session.spot.name+Moment(session.seesion_start).format(' hh:mm DD-MM-YY')} left={LeftContentImage} />
                <Card.Content>
                    <Caption>About:</Caption>
                    <Paragraph>{session.description}</Paragraph>
                </Card.Content>
                <Card.Actions>
                    <Button>Details</Button>
                </Card.Actions>
            </Card>
        </View>
    );
}

const styles = StyleSheet.create({
    item: {
        backgroundColor: '#f9c2ff',
        marginVertical: 8,
        marginHorizontal: 16,
    },
    title: {
        fontSize: 32,
    },
    tinyLogo: {
        borderColor: black,
        borderWidth: 1,
        width: 50,
        height: 50,
    },
});

export default Sessionitem;
