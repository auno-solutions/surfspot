



import { connect } from 'react-redux'
import React, {useEffect, useState} from 'react';
import {
    StatusBar,
    StyleSheet,
    Text,
    View,
    Image,
    TouchableOpacity, Dimensions, ScrollView, ActivityIndicator, SafeAreaView, FlatList, RefreshControl,
} from 'react-native';
import Sessionitem from './sessionItem';
import * as userActions from '../../store/actions/userActions';
import * as systemActions from '../../store/actions/systemActions';



const SessionList = (props) => {
    const [page, setPage] = useState(1);
    const [refreshing, setRefreshing] = React.useState(false);

    useEffect(() => {
        if(props.userReducer.user_data.user_sessions.is_fetched === false && props.userReducer.user_data.user_sessions.fetching === false){
            props.dispatch(userActions.fetchUserSessions(page));
        }
    });

    const onRefresh = React.useCallback(() => {
        props.dispatch(userActions.fetchUserSessions(page));
    });



    return (
            <FlatList
                style={styles.flatContainer}
                refreshControl={
                    <RefreshControl
                        refreshing={refreshing}
                        onRefresh={onRefresh}
                    />
                }


                data={props.userReducer.user_data.user_sessions.data}
                renderItem={(sessionItem) => (
                    <Sessionitem  parent={sessionItem}/>
                )}
            />
    );
}

const styles = StyleSheet.create({
    flatContainer: {
        paddingTop: 20,
    },
    container: {
        flex: 1,
        marginTop: StatusBar.currentHeight || 0,
    },
    item: {
        backgroundColor: '#f9c2ff',
        padding: 20,
        marginVertical: 8,
        marginHorizontal: 16,
    },
    title: {
        fontSize: 32,
    },
});

export default connect(store => {
    return{...store}
})(SessionList);


