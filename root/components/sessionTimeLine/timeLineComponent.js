import { connect } from 'react-redux'
import { Text, View,StyleSheet,Image,RefreshControl } from 'react-native';
import React, { useEffect,useState } from 'react';
import Timeline from 'react-native-timeline-flatlist';
import Moment from 'moment';
import * as userActions from '../../store/actions/userActions';
import { Button,ActivityIndicator, Colors } from 'react-native-paper';

const TimeLineComponent = (props) => {

    function getsportIcon(sportName) {
        switch (sportName) {
            case 'surf_color.png':
                return require('../../assets/sport/surf_color.png');
            case 'kitesurf_color.png':
                return require('../../assets/sport/kitesurf_color.png');
            case 'windsurfing_color.png':
                return require('../../assets/sport/windsurfing_color.png');
            default:
                return require('../../assets/sport/surf_color.png');
        }
    }

    const data =()=> {
        let dataArray = [];
        props.userReducer.user_data.user_sessions.data.map((session)=>{
            dataArray.push({
                time: Moment(session.seesion_start).format('D MMM'),
                title: session.spot.name,
                description: renderDetail(session),
                icon: getsportIcon(session.sport.sportIcon)
            });
        })
        return dataArray;
    }

    const [loading, setLoading] = useState(true);
    const [page, setPage] = useState(1);
    const [timeLinedata, setTimeLinedata] = useState([]);

    useEffect(() => {
        setLoading(true)

        if(props.userReducer.user_data.user_sessions.is_fetched === true){
            setLoading(false)
        }

        if(props.userReducer.user_data.user_sessions.is_fetched === false && props.userReducer.user_data.user_sessions.fetching === false){
            props.dispatch(userActions.fetchUserSessions(page));
        }
        if(timeLinedata.length === 0 && props.userReducer.user_data.user_sessions.is_fetched === true){
            setTimeLinedata(data());
        }


    },[]);

    const showModal = () => {
        props.dispatch({
            type: 'FETCH_SESSIONS_REST',
            payload: false
        });

    };

    const renderDetail = (session)=>{
        return (
            <View style={{flex:1}}>
                <Text>{session.description}</Text>
            </View>
        )
    }

    const refreshControl = ()=>{
        props.dispatch(userActions.fetchUserSessions(page));
    }


    return (
        <View style={styles.container}>
            {/*<Text>Loading*/}
            {/*    <Button icon="map-marker" mode="contained" onPress={showModal}>*/}
            {/*        RESET*/}
            {/*    </Button>*/}
            {/*</Text>*/}
            {loading?
                <ActivityIndicator animating={true} color={Colors.red800} size='large' />
               :
                <Timeline
                    style={styles.list}
                    data={timeLinedata}
                    circleSize={60}
                    circleColor='rgba(0,0,0,0)'
                    lineColor='rgb(45,156,219)'
                    timeContainerStyle={{minWidth:60, marginTop: -5,paddingTop:25}}
                    timeStyle={{textAlign: 'center', backgroundColor:'#50d7b1', color:'white', padding:5, borderRadius:13}}
                    descriptionStyle={{color:'gray'}}
                    iconStyle={{marginLeft: 12}}
                    options={{
                        style:{paddingTop:20},
                        removeClippedSubviews: false,
                        refreshControl: (
                            <RefreshControl
                                refreshing={loading}
                                onRefresh={refreshControl}
                            />
                        ),
                    }}
                    innerCircle={'icon'}
                    detailContainerStyle={{marginLeft:15,marginBottom: 50, paddingLeft: 15, paddingRight: 5, backgroundColor: "#BBDAFF", borderRadius: 10}}
                />
            }

        </View>
    );
}

// const styles = StyleSheet.create({
//     container: {
//         flex: 1,
//         padding: 20,
//         backgroundColor:'white'
//     },
//     list: {
//         flex: 1,
//         marginTop:50,
//     },
// });

const styles = StyleSheet.create({
    container: {
        flex: 1,
        padding: 5,
        paddingRight: 15,
        backgroundColor:'white'
    },
    list: {
        flex: 1,
    },
    title:{
        fontSize:16,
        fontWeight: 'bold'
    },
    descriptionContainer:{
        flexDirection: 'row',
    },
    image:{
        width: 50,
        height: 50,
        borderRadius: 25
    },
    textDescription: {
        marginLeft: 10,
        color: 'gray'
    }
});

export default connect(store => {
    return{...store}
})(TimeLineComponent);


