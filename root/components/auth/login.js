import React, {useEffect, useState} from 'react';
import {connect} from 'react-redux';

import {
    StyleSheet,
    Text,
    TextInput,
    View,
    Button,
    TouchableOpacity,
    Image,
    Alert
} from 'react-native';

import * as authActions from '../../store/actions/authActions'

import Title from 'react-native-paper';
import * as LogBoxStyle from 'react-native/Libraries/LogBox/UI/LogBoxStyle';
import { useNavigation } from '@react-navigation/native';

const LoginScreen = (props) => {
    const [email, setEmail] = useState({value: 'fien@auno.be', error: ''});
    const [password, setPassword] = useState({value: 'test', error: ''});

    const navigation = useNavigation();

    const loginPressed = () => {
        props.dispatch(authActions.fetchUser({email:email,password:password}));
    };

    useEffect(() => {
        if(props.authReducer.user_creation.created === true){
            props.dispatch({
                type: 'CREATE_USER_RESET',
                payload: true,
            });
        }

    }, []);

    const registerButton = (event) => {
        console.log('clicked');

    }

    const purge = () => {
        props.dispatch({
            type: 'PURGE',
            payload: true,
        });
    }

    return (
        <View style={styles.container}>

            <Image style={styles.bgImage} source={{ uri: "https://www.wondersofmaui.com/wp-content/uploads/2015/09/hookipa-beach-empty.jpg" }}/>
            <Text style={styles.titleText} >Surf Spot</Text>
            <View style={styles.inputContainer}>
                <TextInput style={styles.inputs}
                           value={email.value}
                           placeholder="Email"
                           keyboardType="email-address"
                           underlineColorAndroid='transparent'
                           onChangeText={(email) => setEmail({value: email})}/>
            </View>

            <View style={styles.inputContainer}>
                <TextInput style={styles.inputs}
                           placeholder="Password"
                           value={password.value}
                           secureTextEntry={true}
                           underlineColorAndroid='transparent'
                           onChangeText={(password) => setPassword({value: password})}/>
            </View>

            <TouchableOpacity style={styles.btnForgotPassword} onPress={() => this.onClickListener('restore_password')}>
                <Text style={styles.btnText}>Forgot your password?</Text>
            </TouchableOpacity>

            <TouchableOpacity style={[styles.buttonContainer, styles.loginButton]} onPress={() => loginPressed()}>
                <Text style={styles.loginText}>Login</Text>
            </TouchableOpacity>
            <TouchableOpacity style={[styles.buttonContainer, styles.loginButton]} onPress={() => purge()}>
                <Text style={styles.loginText}>Purge</Text>
            </TouchableOpacity>

            <TouchableOpacity style={styles.buttonContainer}
                              onPress={() => {
                                  navigation.navigate('Register');
                              }}>
                <Text style={styles.btnText}>Register</Text>
            </TouchableOpacity>
        </View>

    );
};

const resizeMode = 'center';

const styles = StyleSheet.create({
    titleText: {
        color: 'white',
        fontSize: 65,
        fontWeight: '600',
        includeFontPadding: false,
        // lineHeight: 20,
    },
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#DCDCDC',
    },
    inputContainer: {
        borderBottomColor: '#F5FCFF',
        backgroundColor: '#FFFFFF',
        borderRadius:30,
        borderBottomWidth: 1,
        width:300,
        height:45,
        marginBottom:20,
        flexDirection: 'row',
        alignItems:'center',

        shadowColor: "#808080",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,

        elevation: 5,
    },
    inputs:{
        height:45,
        marginLeft:16,
        borderBottomColor: '#FFFFFF',
        flex:1,
    },
    inputIcon:{
        width:30,
        height:30,
        marginRight:15,
        justifyContent: 'center'
    },
    buttonContainer: {
        height:45,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        marginBottom:20,
        width:300,
        borderRadius:30,
        backgroundColor:'transparent'
    },
    btnForgotPassword: {
        height:15,
        flexDirection: 'row',
        justifyContent: 'flex-end',
        alignItems: 'flex-end',
        marginBottom:10,
        width:300,
        backgroundColor:'transparent'
    },
    loginButton: {
        backgroundColor: "#00b5ec",

        shadowColor: "#808080",
        shadowOffset: {
            width: 0,
            height: 9,
        },
        shadowOpacity: 0.50,
        shadowRadius: 12.35,

        elevation: 19,
    },
    loginText: {
        color: 'white',
    },
    bgImage:{
        flex: 1,
        position: 'absolute',
        width: '100%',
        height: '100%',
        justifyContent: 'center',
    },
    btnText:{
        color:"white",
        fontWeight:'bold'
    }
});

export default connect(state => {
    return {...state};
})(LoginScreen);
