import {connect} from 'react-redux';
import React, {useEffect, useState} from 'react';
import {StyleSheet,Text,View,TextInput,Button,TouchableOpacity,Image,Alert,ActivityIndicator} from 'react-native';
import { useNavigation } from '@react-navigation/native';

import * as authActions from '../../store/actions/authActions'


const RegisterScreen = (props) => {
    const [username, setUsername] = useState({value: 'aubri', error: ''});
    const [email, setEmail] = useState({value: 'aubri@auno.be', error: ''});
    const [password, setPassword] = useState({value: 'test', error: ''});
    const navigation = useNavigation();

    const onClickListener = (viewId) => {

        props.dispatch({
            type: 'CREATE_USER_LOAD',
            payload: true,
        });
        props.dispatch(authActions.createUser({username:username,email:email,password:password}));
    };

    useEffect(() => {
        isUserCreated();

    });

    const isUserCreated =()=> {
        if(props.authReducer.user_creation.created === true){
            navigation.navigate('SignIn');
        }
    }

    if(props.authReducer.user_creation.is_fetching){
        return (
            <View style={styles.container}>
                <Image style={styles.bgImage}
                       source={{uri: 'https://www.windsurfingnowmag.com/wp-content/uploads/2015/11/AC15_wv_Sarah_Bottom_turn.jpg'}}/>
                <View >
                    <ActivityIndicator size="large" color="#00ff00" />
                    <Text>Loading</Text>
                </View>
            </View>
        )
    }

    return (
        <View style={styles.container}>



            <Image style={styles.bgImage}
                   source={{uri: 'https://www.windsurfingnowmag.com/wp-content/uploads/2015/11/AC15_wv_Sarah_Bottom_turn.jpg'}}/>
            <View style={styles.inputContainer}>
                <TextInput style={styles.inputs}
                           placeholder="Username"
                           value={username.value}
                           underlineColorAndroid='transparent'
                           onChangeText={(username) => setEmail({value: username})}/>
                <Image style={styles.inputIcon}
                       source={{uri: 'https://img.icons8.com/color/40/000000/circled-user-male-skin-type-3.png'}}/>
            </View>

            <View style={styles.inputContainer}>
                <TextInput style={styles.inputs}
                           placeholder="Email"
                           value={email.value}
                           keyboardType="email-address"
                           underlineColorAndroid='transparent'
                           onChangeText={(email) => setEmail({value: email})}/>
                <Image style={styles.inputIcon}
                       source={{uri: 'https://img.icons8.com/flat_round/40/000000/secured-letter.png'}}/>
            </View>

            <View style={styles.inputContainer}>
                <TextInput style={styles.inputs}
                           placeholder="Password"
                           value={password.value}
                           secureTextEntry={true}
                           underlineColorAndroid='transparent'
                           onChangeText={(password) => setPassword({value: password})}/>
                <Image style={styles.inputIcon} source={{uri: 'https://img.icons8.com/color/40/000000/password.png'}}/>
            </View>

            {props.authReducer.user_creation.error === true?
                <View style={styles.inputContainer}>
                    <Text>{props.authReducer.user_creation.message}</Text>
                </View>
                :
                null
            }

            <TouchableOpacity style={styles.btnByRegister} onPress={() => this.onClickListener('restore_password')}>
                <Text style={styles.textByRegister}>By registering on this App you confirm that you have read and accept
                    our policy</Text>
            </TouchableOpacity>

            <TouchableOpacity style={[styles.buttonContainer, styles.loginButton]}
                              onPress={() => onClickListener('login')}>
                <Text style={styles.loginText}>Register</Text>
            </TouchableOpacity>


            <TouchableOpacity style={styles.buttonContainer}
                              onPress={() => {
                                  navigation.navigate('SignIn');
                              }}>
                <Text style={styles.btnText}>Have an account?</Text>
            </TouchableOpacity>
        </View>
    );
};

const resizeMode = 'center';

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#DCDCDC',
    },
    inputContainer: {
        borderBottomColor: '#F5FCFF',
        backgroundColor: '#FFFFFF',
        borderRadius: 30,
        borderBottomWidth: 1,
        width: 300,
        height: 45,
        marginBottom: 20,
        flexDirection: 'row',
        alignItems: 'center',

        shadowColor: '#808080',
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,

        elevation: 5,
    },
    inputs: {
        height: 45,
        marginLeft: 16,
        borderBottomColor: '#FFFFFF',
        flex: 1,
    },
    inputIcon: {
        width: 30,
        height: 30,
        marginRight: 15,
        justifyContent: 'center',
    },
    buttonContainer: {
        height: 45,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        marginBottom: 20,
        width: 300,
        borderRadius: 30,
        backgroundColor: 'transparent',
    },
    btnByRegister: {
        height: 15,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        marginVertical: 20,
        width: 300,
        backgroundColor: 'transparent',
    },
    loginButton: {
        backgroundColor: '#00b5ec',

        shadowColor: '#808080',
        shadowOffset: {
            width: 0,
            height: 9,
        },
        shadowOpacity: 0.50,
        shadowRadius: 12.35,

        elevation: 19,
    },
    loginText: {
        color: 'white',
    },
    bgImage: {
        flex: 1,
        position: 'absolute',
        width: '100%',
        height: '100%',
        justifyContent: 'center',
    },
    btnText: {
        color: 'white',
        fontWeight: 'bold',
        textShadowColor: 'rgba(0, 0, 0, 0.75)',
        textShadowOffset: {width: -1, height: 1},
        textShadowRadius: 10,
    },
    textByRegister: {
        color: 'white',
        fontWeight: 'bold',
        textAlign: 'center',

        textShadowColor: 'rgba(0, 0, 0, 0.75)',
        textShadowOffset: {width: -1, height: 1},
        textShadowRadius: 10,
    },
});

export default connect(store => {
    return {...store};
})(RegisterScreen);


