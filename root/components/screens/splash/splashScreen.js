



import { connect } from 'react-redux'
import { Text, View } from 'react-native';
import React, { useEffect } from 'react';
import AnimatedSplash from "react-native-animated-splash-screen";

import * as systemActions from '../../../store/actions/systemActions'
import * as authActions from '../../../store/actions/authActions';


const AuthMiddleware = (props) => {

    useEffect(() => {
        console.log(props);


        // props.dispatch(systemActions.fethUserData());

        // props.dispatch({
        //     type: 'TEST',
        //     payload: 'test'
        // });

    },[]);

    return (
        <AnimatedSplash
            translucent={true}
            isLoaded={props.systemReducer.isLoading_system !== true && props.systemReducer.fetching_data !== true}
            logoImage={require("../../../assets/arrow_back.png")}
            backgroundColor={"#bfffff"}
            logoHeight={150}
            logoWidth={150}
        >
        </AnimatedSplash>
    );
}

export default connect(store => {
    return{...store}
})(AuthMiddleware);


