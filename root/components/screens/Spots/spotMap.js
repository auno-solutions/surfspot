import CarouselSpotItem from '../../utils/carouselSpotItem'
import React, {useEffect} from 'react';
import {
    StyleSheet,
    View,
    Text,
    Dimensions,
    TouchableOpacity,
    Alert,
} from 'react-native';
import MapView, {
    Marker,
    Callout,
    CalloutSubview,
    ProviderPropType,
} from 'react-native-maps';
import CustomCallout from './markerCallout';

import Carousel from 'react-native-snap-carousel';
import {IconButton, Avatar, Button, Card, Title, Paragraph, Surface, Colors} from 'react-native-paper';
import Geolocation from '@react-native-community/geolocation';
import * as systemActions from '../../../store/actions/systemActions';
import {connect} from 'react-redux';
import {useNavigation} from '@react-navigation/native';
import * as userActions from '../../../store/actions/userActions';
import axios from 'axios';

const {width: viewportWidth, height: viewportHeight} = Dimensions.get('window');

function wp(percentage) {
    const value = (percentage * viewportWidth) / 100;
    return Math.round(value);
}

const slideHeight = viewportHeight * 0.36;
const slideWidth = wp(75);
const itemHorizontalMargin = wp(2);

const sliderWidth = viewportWidth;
const itemWidth = slideWidth + itemHorizontalMargin * 2;

const LeftContent = props => <Avatar.Icon {...props} icon="folder"/>;

const {width, height} = Dimensions.get('window');
const ASPECT_RATIO = width / height;
const LATITUDE_DELTA = 0.0922;
const LONGITUDE_DELTA = LATITUDE_DELTA * ASPECT_RATIO;

const SPOTRANGE = 20; //in km

const mapRef = React.createRef();

class SpotMap extends React.Component {

    constructor(props) {

        super(props);
        let userLocation = {is_set: false, latitude: 0, longitude: 0, spotRange: SPOTRANGE};
        this.state = {
            userLocation: userLocation,
            userClosestSpots: {
                loading: false,
                is_fetched: false,
                data: [],
            },
            region: {
                // latitude: 0,
                // longitude: 0,
                latitude: 51.2163,
                longitude: 2.8883,
                latitudeDelta: LATITUDE_DELTA,
                longitudeDelta: LONGITUDE_DELTA,
            },
            markers: [],
        };

        if (this.state.userLocation.is_set === false) {
            this.updateGeoLocation();
        }
    }


    updateGeoLocation = () => {
        Geolocation.getCurrentPosition((info) => {
                mapRef.current.animateToRegion({
                    latitude: info.coords.latitude,
                    longitude: info.coords.longitude,
                    latitudeDelta: LATITUDE_DELTA,
                    longitudeDelta: LONGITUDE_DELTA,
                }, 1000);

            this.setState({
                ...this.state,
                userLocation: {
                    latitude: info.coords.latitude,
                    longitude: info.coords.longitude,
                    // latitude: 51.2163,
                    // longitude: 2.8883,
                    spotRange: SPOTRANGE,
                    is_set: true,
                },
                region: {
                    ...this.state.region,
                    latitude: info.coords.latitude,
                    longitude: info.coords.longitude,
                },
            });

            },
        (error) => this.setState({ error: error.message }),
            { enableHighAccuracy: false, timeout: 5000, maximumAge: 10000 },

        );
    };


    fetchItems = async () => {

        if (this.state.userClosestSpots.data.length === 0 && this.state.userLocation.is_set === true) {
            let response = await systemActions.fetchNearesSpots(this.state.userLocation);
            if (Array.isArray(response) && response.length > 0) {
                this.setState({
                        ...this.state,
                        userClosestSpots: {
                            loading: false,
                            is_fetched: true,
                            data: response,
                        },
                    },
                );
                return response;
            }
        }
    };


    getItems = () => {
        if (this.state.userClosestSpots.is_fetched === true) {
            return this.state.userClosestSpots.data;
        } else {
            this.fetchItems();
            return [];
        }
    };

    _renderItem = ({item, index}) => {
        return (
            <CarouselSpotItem spot={item} map={mapRef} latitudeDelta={LATITUDE_DELTA} longitudeDelta={LONGITUDE_DELTA}
            />
        )
    };

    render() {
        const {region, markers} = this.state;
        return (
            <View style={styles.container}>
                <MapView
                    ref={mapRef}
                    provider={this.props.provider}
                    style={styles.map}
                    initialRegion={region}
                    zoomTapEnabled={false}
                    showsMyLocationButton={true}
                    showsUserLocation={true}
                    followsUserLocation={true}
                >

                    {this.state.userClosestSpots.data.map((spot) => {

                        // description: "Raversijde"
                        // distance: "3.0530588876132643"
                        // id: "2"
                        // lattitude: "51.206767"
                        // likes: "0"
                        // longitude: "2.857431"
                        // name: "Rave party"
                        // user_id: "5"


                        return (
                            <Marker
                                key={spot.id + '-' + spot.user_id}
                                coordinate={
                                    {
                                        latitude: parseFloat(spot.lattitude),
                                        longitude: parseFloat(spot.longitude),
                                    }
                                }
                                calloutOffset={{x: -8, y: 28}}
                                calloutAnchor={{x: 0.5, y: 0.4}}
                                image={require('../../../assets/surfspot-icon.png')}

                            >
                                <Callout
                                    alphaHitTest
                                    tooltip
                                    onPress={e => {
                                        if (
                                            e.nativeEvent.action === 'marker-inside-overlay-press' ||
                                            e.nativeEvent.action === 'callout-inside-press'
                                        ) {
                                            return;
                                        }

                                        Alert.alert('callout pressed');
                                    }}
                                    style={styles.customView}
                                >
                                    <CustomCallout spot={spot}>
                                        <CalloutSubview
                                            onPress={() => {
                                                console.log('go to detail');
                                            }}
                                            style={[styles.calloutButton]}
                                        >
                                        </CalloutSubview>
                                    </CustomCallout>
                                </Callout>
                            </Marker>
                        );
                    })}

                    {/*<Marker*/}
                    {/*    coordinate={this.state.userLocation}*/}
                    {/*    title={'I am here'}*/}
                    {/*    description={'Avast Ye!'}*/}
                    {/*    image={require('../../../assets/user-mappin.png')}*/}
                    {/*/>*/}


                </MapView>
                <View style={styles.buttonCallout}>
                    <IconButton
                        icon="map-plus"
                        color={Colors.white}
                        size={30}
                        onPress={() =>{
                            this.props.navigation.navigate('New Spot');
                        }}
                    />
                </View>

                <View style={styles.buttonContainer}>

                    <Carousel
                        layout={'default'}
                        ref={(c) => {
                            this._carousel = c;
                        }}
                        data={this.getItems()}
                        renderItem={this._renderItem}
                        sliderWidth={viewportWidth}
                        itemWidth={itemWidth}
                    />
                </View>
            </View>
        );
    }
}

SpotMap.propTypes = {
    provider: ProviderPropType,
};

const styles = StyleSheet.create({
    buttonCallout: {
        flex: 1,
        flexDirection:'row',
        position:'absolute',
        top:10,
        right:10,
        alignSelf: "center",
        justifyContent: "space-between",
        backgroundColor: Colors.red500,
        // borderWidth: 0.5,
        borderRadius: 30
    },
    cardButtonBox: {
        flexDirection: 'row',
        justifyContent: 'space-evenly',

    },
    customView: {
        width: 140,
        height: 80,
    },
    cardSubTitle: {
        marginLeft: -18,
        marginTop: -35,
        marginBottom: -20,
    },
    plainView: {
        width: 60,
    },
    container: {
        ...StyleSheet.absoluteFillObject,
        justifyContent: 'flex-end',
        alignItems: 'center',
    },
    map: {
        ...StyleSheet.absoluteFillObject,
    },
    bubble: {
        flex: 1,
        backgroundColor: 'rgba(255,255,255,0.7)',
        paddingHorizontal: 18,
        paddingVertical: 12,
        borderRadius: 20,
    },
    latlng: {
        width: 200,
        alignItems: 'stretch',
    },
    button: {
        width: 80,
        paddingHorizontal: 12,
        alignItems: 'center',
        marginHorizontal: 10,
    },
    addButtonContainer: {
        top: 200,
        // height: 100,
    },
    buttonContainer: {
        flexDirection: 'row',
        marginVertical: 20,
        backgroundColor: 'transparent',
    },
    calloutButton: {
        width: 'auto',
        backgroundColor: 'rgba(255,255,255,0.7)',
        paddingHorizontal: 6,
        paddingVertical: 6,
        borderRadius: 12,
        alignItems: 'center',
        marginHorizontal: 10,
        marginVertical: 10,
    },
    cardHeading: {
        flexDirection: 'row',
        justifyContent: 'space-between',
    },
});

export default connect(store => {
    return {...store};
})(SpotMap);


// export default SpotMap;





