import React, {useEffect, useState} from 'react';
import {createMaterialTopTabNavigator} from '@react-navigation/material-top-tabs';

import SpotMap from './spotMap';
import MySpotScreen from './mySpotScreen';
import MySpotScreen2 from './mySpotScreen2';
import {createStackNavigator} from '@react-navigation/stack';

const SpotTabs = (props) => {

    const [swipeEnabled, setSwipeEnabled] = useState(true);

    const Tab = createMaterialTopTabNavigator();
    const Stack = createStackNavigator();

    return (
        <Tab.Navigator
            swipeEnabled={swipeEnabled}
            screenOptions={({ navigation, route }) => {
                if (route.name === 'Map' && navigation.isFocused()) {
                    setSwipeEnabled(false);
                } else if (route.name !== 'Map' && navigation.isFocused()) {
                    setSwipeEnabled(true);
                }
            }}
        >
            <Tab.Screen name="MySpots" component={MySpotScreen2}/>
            <Tab.Screen name="Map" component={SpotMap}/>
        </Tab.Navigator>
    );
};

export default SpotTabs

// export default connect(store => {
//     return {...store};
// })(SpotTabs);


