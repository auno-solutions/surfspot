import {connect} from 'react-redux';
import {ScrollView, StyleSheet, Text, View} from 'react-native';
import React, {useEffect} from 'react';
import {HelperText, Switch, Card, Surface, TextInput, Title, Button} from 'react-native-paper';
import AddSpotMap  from './addSpotMap';
import {useNavigation} from '@react-navigation/native';

import * as userActions from './../../../store/actions/userActions'


const spotDetail = (props) => {
    const [marker, setMarker] = React.useState(undefined);

    const [spot, setSpot] = React.useState({
        name: '',
        description: '',
        user: "/api/users/"+props.authReducer.user.data.id,
        isPublic: true,
        lattitude: 0,
        longitude: 0,
        likes: 0,
    });

    const [isSwitchOn, setIsSwitchOn] = React.useState(true);
    const onToggleSwitch = () => setIsSwitchOn(!isSwitchOn);
    const navigation = useNavigation();

    useEffect(() => {

        // props.dispatch({
        //     type: 'TEST',
        //     payload: 'test'
        // });

    }, []);

    const setSelectedCoords = (coords)=>{
        setMarker(coords);
        setSpot({...spot,lattitude: coords.latlng.latitude,longitude:coords.latlng.latitude})
    }

    function createUserSpot() {
        setSpot({...spot,isPublic: isSwitchOn})
        props.dispatch(userActions.createNewSpot(spot));
        navigation.navigate('MySpots');
    }


    return (
        <ScrollView style={styles.container}>
            <View style={styles.formContainer}>
                <Surface style={styles.surface}>
                    <Card.Title title="Spot location"/>
                    <AddSpotMap updateItem={setSelectedCoords} selectedMarker={marker}/>
                    <Card.Title style={styles.cardSubTitle} title="" subtitle="Click the button to show the map"/>
                    {marker !== undefined?
                        <Card.Title style={styles.cardSubTitle} title="" subtitle={spot.lattitude +'   -   '+ spot.longitude}/>
                        :
                        null
                    }
                </Surface>
                <Surface style={styles.surfaceForm}>
                    <Card.Title title="Spot Info"/>
                    <TextInput
                        style={styles.inputStyle}
                        mode='outlined'
                        label="Spot Name"
                        value={spot.name}
                        onChangeText={text => setSpot({...spot,name: text})}
                    />

                    <TextInput
                        style={styles.inputStyle}
                        mode='outlined'
                        label="Description"
                        value={spot.description}
                        onChangeText={text => setSpot({...spot,description: text})}
                    />

                    <View style={styles.checkBoxForm}>
                        <Text style={styles.checkboxLabel}>Public Spot</Text>
                        <Switch style={styles.inputStyle} value={isSwitchOn} onValueChange={onToggleSwitch} />
                    </View>


                </Surface>
                <Surface style={styles.surfaceForm}>
                    <Card.Title title="Spot rating"/>
                </Surface>
                <Surface style={styles.surfaceForm}>
                    <Card.Title title="Add pictures"/>
                </Surface>

                <View style={styles.buttonGroup}>
                    <Button icon="check" mode="contained" onPress={() => createUserSpot()}>
                        Create
                    </Button>
                </View>
            </View>
        </ScrollView>
    );
};

const styles = StyleSheet.create({
    buttonGroup: {
        marginTop:15,
        justifyContent: 'space-evenly',
    },
    textColor: {
        color: 'white',
    },
    container: {
        backgroundColor: '#f8f8f8',
        flex: 1,
        zIndex: 1,
    },
    checkBoxForm:{
        flex:1,
        flexDirection:'row',
        justifyContent: 'flex-end',
    },
    checkboxLabel: {
        marginTop: 13,
        marginLeft: 15
    },
    formContainer: {
        margin: 20,
    },
    formDivider: {
        height: 20,
    },
    inputStyle: {
        margin: 10,
        backgroundColor: '#ffffff',

    },
    cardSubTitle: {
        marginTop: -25,
        marginBottom: -20,

    },
    chipStyle: {
        marginTop: 15,
    },
    surfaceForm: {
        flex: 1,
        marginTop: 15,
        elevation: 10,
    },

    surface: {
        marginTop: 15,
        alignItems: 'center',
        justifyContent: 'center',
        elevation: 10,
    },
});


export default connect(store => {
    return {...store};
})(spotDetail);


