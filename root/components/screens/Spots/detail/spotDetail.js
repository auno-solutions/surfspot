import {connect} from 'react-redux';
import {
    StyleSheet,
    Text,
    View,
    Image,
    TouchableOpacity, Dimensions, ScrollView, ActivityIndicator,
} from 'react-native';
import {
    TextInput,
    Button,
    Title,
    Subheading,
    Card,
    Chip,
    ToggleButton,
    Portal,
    List,
    Surface,
} from 'react-native-paper';
import React, {useEffect} from 'react';
import {WebView} from 'react-native-webview';

import MapView, {
    Marker,
    Callout,
    CalloutSubview,
    ProviderPropType,
} from 'react-native-maps';
import {fetchSpotDetails} from '../../../../store/actions/systemActions';
import * as systemActions from '../../../../store/actions/systemActions';

const {width, height} = Dimensions.get('window');
const ASPECT_RATIO = width / height;
const LATITUDE_DELTA = 0.0922;
const LONGITUDE_DELTA = LATITUDE_DELTA * ASPECT_RATIO;

const myHtmlFile = require('../../../../assets/forecast/windguru.html');
import LeaderBoard from '../leaderBoard/leaderBoard';
import SpotDetailHeader from './spotDetailHeader';

const SpotDetail = (props) => {
    const [spot, setSpot] = React.useState(props.route.params.spot);
    const [spotDetails, setSpotDetails] = React.useState(undefined);

    useEffect(() => {

    }, []);


    useEffect(() => {
        //need to remove "lattitude" spelling mistake
        props.route.params.spot.latitude = props.route.params.spot.latitude === undefined? props.route.params.spot.lattitude: props.route.params.spot.latitude;

        if (spotDetails === undefined) {
            loadSpotDetails();
        }

        props.navigation.setOptions({
            title: props.route.params.spot.name,
        });

    }, [props.navigation]);

    async function loadSpotDetails() {
        let response = await fetchSpotDetails(props.route.params.spot);
        setSpotDetails(response);

    }


    return (
        <ScrollView>

            <View style={styles.container}>
                <View style={styles.header}>
                    <View style={styles.headerContent}>
                        <SpotDetailHeader/>

                    {/*    <Text style={styles.name}>*/}
                    {/*        {spot.name}*/}
                    {/*    </Text>*/}
                    </View>
                </View>

                <View style={styles.profileDetail}>
                    <View style={styles.detailContent}>
                        <Text style={styles.title}>Surfers</Text>
                        <Text style={styles.count}>
                            {spotDetails !== undefined ?
                                spotDetails.user_total
                                :
                                <View>
                                    <ActivityIndicator size="large" color="#00ff00"/>
                                </View>
                            }
                        </Text>
                    </View>
                    <View style={styles.detailContent}>
                        <Text style={styles.title}>Sessions</Text>
                        <Text style={styles.count}>
                            {spotDetails !== undefined ?
                                spotDetails.session_total
                                :
                                <View>
                                    <ActivityIndicator size="large" color="#00ff00"/>
                                </View>
                            }

                        </Text>
                    </View>
                    <View style={styles.detailContent}>
                        <Text style={styles.title}>likes</Text>
                        <Text style={styles.count}>
                            {spotDetails !== undefined ?
                                spotDetails.likes
                                :
                                <View>
                                    <ActivityIndicator size="large" color="#00ff00"/>
                                </View>
                            }
                        </Text>
                    </View>
                </View>
                <View style={styles.pageView}>
                    {/*<View style={styles.mapHeader}>*/}
                    {/*    <Title style={styles.leaderBoardHeader}>Forecast</Title>*/}
                    {/*</View>*/}
                    {/*<View style={styles.windguruView}>*/}

                    {/*    <WebView javaScriptEnabled={true}*/}
                    {/*             injectedJavaScript={`const meta = document.createElement('meta'); meta.setAttribute('content', 'width=width, initial-scale=0.5, maximum-scale=0.5, user-scalable=3.0'); meta.setAttribute('name', 'viewport'); document.getElementsByTagName('head')[0].appendChild(meta); `}*/}
                    {/*             scalesPageToFit={false}*/}
                    {/*             containerStyle={{width: width}} source={{uri: 'file:///android_asset/windguru.html'}}/>*/}


                    {/*</View>*/}
                    <View style={styles.leaderBoardStyle}>
                        <Title style={styles.leaderBoardHeader}>Leaderboard</Title>
                        <LeaderBoard spot={props.route.params.spot}/>
                    </View>


                    <View style={styles.mapHeader}>
                        <Title style={styles.leaderBoardHeader}>Spot</Title>
                    </View>
                    <View style={styles.surfaceMap}>

                        <MapView
                            provider={props.provider}
                            style={styles.map}
                            initialRegion={{
                                latitude: parseFloat(props.route.params.spot.latitude),
                                longitude: parseFloat(props.route.params.spot.longitude),
                                latitudeDelta: LATITUDE_DELTA,
                                longitudeDelta: LONGITUDE_DELTA,
                            }}
                            zoomTapEnabled={false}
                            scrollEnabled={false}
                        >
                            <Marker
                                key={spot.id + '-' + spot.user_id}
                                coordinate={
                                    {
                                        latitude: parseFloat(props.route.params.spot.latitude),
                                        longitude: parseFloat(props.route.params.spot.longitude),
                                    }
                                }
                                calloutOffset={{x: -8, y: 28}}
                                calloutAnchor={{x: 0.5, y: 0.4}}
                                image={require('../../../../assets/surfspot-icon.png')}
                            />
                        </MapView>

                    </View>
                </View>

            </View>
        </ScrollView>
    );
};

SpotDetail.propTypes = {
    provider: ProviderPropType,
};


const styles = StyleSheet.create({
    mapHeader:{
        marginTop:30,
        elevation:4,
    },
    pageView: {
        backgroundColor: 'white',
        flex: 1,
    },
    container: {
        flex: 1,
    },
    map: {
        height: 250,
        ...StyleSheet.absoluteFillObject,
    },
    windguruView: {
        overflow: 'hidden',
        alignItems: 'center',
        justifyContent: 'center',
        height: 220,
    },

    surface: {
        overflow: 'hidden',
        alignItems: 'center',
        justifyContent: 'center',
        elevation: 4,
        marginTop: 90,
        margin: 20,
        height: 220,
        borderRadius: 20,
    },
    leaderBoardHeader: {
        elevation: 5,
        height: 30,
        textAlign: 'center',
        width: 400,
        backgroundColor: '#00CED1',

    },
    leaderBoardStyle: {
        overflow: 'hidden',
        alignItems: 'center',
        justifyContent: 'center',
    },
    surfaceMap: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        marginTop: -1,
        height: 220,
    },
    header: {
        backgroundColor: '#00CED1',
    },
    headerContent: {
        flex: 1,
        padding: 30,
        alignItems: 'center',
        height: 250,
    },
    avatar: {
        width: 130,
        height: 130,
        borderRadius: 63,
        borderWidth: 4,
        borderColor: 'white',
        marginBottom: 10,
    },
    name: {
        fontSize: 22,
        color: '#FFFFFF',
        fontWeight: '600',
    },
    profileDetail: {
        width: width,
        flex: 1,
        alignSelf: 'center',
        marginTop:-10,
        alignItems: 'center',
        flexDirection: 'row',
        justifyContent: 'space-evenly',
        backgroundColor: '#ffffff',
        borderTopRightRadius: 20,
        borderTopLeftRadius: 20,

    },
    detailContent: {
        margin: 10,
        alignItems: 'center',
    },
    title: {
        fontSize: 20,
        color: '#00CED1',
    },
    count: {
        fontSize: 18,
    },
    bodyContent: {
        flex: 1,
        alignItems: 'center',
        padding: 30,
        marginTop: 40,
    },
    textInfo: {
        fontSize: 18,
        marginTop: 20,
        color: '#696969',
    },
    buttonContainer: {
        marginTop: 10,
        height: 45,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        marginBottom: 20,
        width: 250,
        borderRadius: 30,
        backgroundColor: '#00CED1',
    },
    description: {
        fontSize: 20,
        color: '#00CED1',
        marginTop: 10,
        textAlign: 'center',
    },
});


export default connect(store => {
    return {...store};
})(SpotDetail);


