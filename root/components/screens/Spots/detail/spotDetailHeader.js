import {connect} from 'react-redux';
import React, {useEffect} from 'react';
import {
    StyleSheet,
    Text,
    View,
    Image,
    Dimensions,
    ScrollView,
    TouchableOpacity,
    Alert,
    ImageBackground
} from 'react-native';

import Carousel, { Pagination } from 'react-native-snap-carousel';
import {Button, Card, Title} from 'react-native-paper';

const { width: viewportWidth, height: viewportHeight } = Dimensions.get('window');

const SpotDetailHeader = (props) => {
        function wp (percentage) {
            const value = (percentage * viewportWidth) / 100;
            return Math.round(value);
        }

        const slideHeight = viewportHeight * 0.36;
        const slideWidth = wp(75);
        const itemHorizontalMargin = wp(2);

        const sliderWidth = viewportWidth;
        const itemWidth = viewportWidth;


        const ENTRIES1 = [
            {
                illustration: 'https://upload.wikimedia.org/wikipedia/commons/1/13/Windsurfer_met_zijn_set_in_Pozo_Izquierdo.jpg',
            },
            {
                illustration: 'https://media-cdn.tripadvisor.com/media/photo-s/02/eb/3f/7b/playa-de-pozo-izquierdo.jpg',
            },
            {
                default: true,
            },
        ];
        function _renderItem({item, index}){
            if(item.default){
                return (

                    <View style={styles.slide}>
                        <ImageBackground source={require('../../../../assets/spot/placeholder.jpg')} style={styles.image}/>
                        <Button color='#00CED1' icon="camera" mode="text" style={styles.overlayButton} onPress={() => console.log('Pressed')}>
                            Add new
                        </Button>
                    </View>
                );
            }
            return (
                <View style={styles.slide}>
                    <ImageBackground source={{uri: item.illustration}} style={styles.image}/>
                </View>

            );
        }
        useEffect(() => {

        }, []);

        const [activeSlide, setActiveSlide] = React.useState(0);

        return (
            <View>
                <Carousel
                    layout={'default'}
                    // ref={(c) => { this._carousel = c; }}
                    data={ENTRIES1}
                    renderItem={_renderItem}
                    sliderWidth={viewportWidth}
                    itemWidth={itemWidth}
                    onSnapToItem={(index) => setActiveSlide(index) }
                    containerCustomStyle={styles.carou}
                />
                <Pagination
                    dotsLength={ENTRIES1.length}
                    activeDotIndex={activeSlide}
                    containerStyle={{marginTop:10,paddingTop:5,paddingBottom:5, marginBottom:-10 }}
                    dotContainerStyle={{height:5}}
                    dotStyle={{
                        width: 10,
                        height: 10,
                        borderRadius: 5,
                        marginHorizontal: 8,
                        backgroundColor: 'rgba(255,5,5,0.92)'
                    }}
                    inactiveDotStyle={{
                        // Define styles for inactive dots here
                    }}
                    inactiveDotOpacity={0.4}
                    inactiveDotScale={0.6}
                />
            </View>
        );
    };

const styles = StyleSheet.create({
    overlayButton:{
        position: 'absolute',
        bottom: 55,
        width:150,
        left: viewportWidth /2 - 75,
        color: '#00CED1',

    },
    carou: {
        marginTop:-50,
        marginBottom:-50,
    },
    slide: {
        flex:1,
        width: viewportWidth,
        paddingHorizontal: 0
        // other styles for the item container
    },
    container: {
        height:25,
        flex: 1,
    },
    image: {
        width: viewportWidth,
        flex: 1,
    },
});


export default connect(store => {
    return {...store};
})(SpotDetailHeader);


