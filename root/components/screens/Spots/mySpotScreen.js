import {connect} from 'react-redux';
import {
    ScrollView,
    StyleSheet,
    Text,
    View,
    Dimensions,
    Image,
    TouchableOpacity,
    ActivityIndicator, RefreshControl,
} from 'react-native';

import React, {useEffect, useState} from 'react';
import {Surface, Avatar, Card, Title, Paragraph, TouchableRipple, Headline, Button} from 'react-native-paper';
import Icon from 'react-native-vector-icons/Ionicons';
import {useNavigation} from '@react-navigation/native';
import * as systemActions from '../../../store/actions/systemActions';

const {width, height} = Dimensions.get('window');
const SCREEN_HEIGHT = height;
const SCREEN_WIDTH = width;
const ASPECT_RATIO = width / height;

import { useFocusEffect } from '@react-navigation/native';
import * as userActions from '../../../store/actions/userActions';


const MySpotScreen = (props) => {
    const navigation = useNavigation();
    const [listCount, setListCount] = useState(props.userReducer.user_data.user_spots.data.length);
    const [refreshing, setRefreshing] = React.useState(false);

    let userLocation = {latitude: 0, longitude: 0};

    useEffect(() => {
        if (props.userReducer.user_data.user_spots.fetching === false && props.userReducer.user_data.user_spots.is_fetched === false) {
            props.dispatch(systemActions.fethUserSpots());
        }
    },[]);

    function createSession(spot) {
        navigation.navigate('New Session', {spot: spot});
    }

    function goTodetail(spot) {
        navigation.navigate('Spot detail', {spot: spot});

    }

    const onRefresh = React.useCallback(() => {
        props.dispatch(systemActions.fethUserSpots());
    }, []);

    return (




        <ScrollView style={styles.container}

                    refreshControl={
                        <RefreshControl
                            refreshing={refreshing}
                            onRefresh={onRefresh}
                        />
                    }
        >
            <View style={styles.top}>
                {listCount === 0 &&
                    <Card>
                        <Card.Content>
                            <Title>Hello there!</Title>
                            <Paragraph>You do not have any saved spots. You can add new spots to you list in the map!</Paragraph>
                        </Card.Content>
                        <Card.Actions style={{alignSelf: 'center'}}>
                            <Button style={{width: '100%'}} mode="contained" onPress={() => navigation.navigate('Map')}>Go to map</Button>

                        </Card.Actions>
                    </Card>
                }

                {props.userReducer.user_data.user_spots.is_fetched === false ?
                    <View>
                        <ActivityIndicator size="large" color="#00ff00"/>
                        <Text>Loading</Text>
                    </View>
                    :
                    props.userReducer.user_data.user_spots.data.map((spot) => {
                        return (
                            <Surface key={'spot-' + spot.id} style={styles.spotConttainer}>
                                <View style={styles.iconContainerStyle}>
                                    <TouchableOpacity
                                        onPress={() => createSession(spot)}
                                    >
                                        <Icon
                                            iconStyle={styles.iconStyle}
                                            reverse
                                            size={50}
                                            name='add-circle'
                                            color='#517fa4'
                                        />
                                    </TouchableOpacity>
                                </View>
                                <TouchableOpacity
                                    onPress={() => goTodetail(spot)}
                                >
                                    <Image
                                        style={styles.imageContainer}
                                        source={{uri: 'https://www.wondersofmaui.com/wp-content/uploads/2015/09/hookipa-beach-empty.jpg'}}
                                    />
                                    <View style={styles.caption}>
                                        <Text style={styles.captionText}>
                                            {spot.name}
                                        </Text>
                                        <Text style={styles.descriptionText}>
                                            0 active
                                        </Text>
                                    </View>
                                </TouchableOpacity>
                            </Surface>
                        );
                    })
                }
            </View>

        </ScrollView>
    );
};

const styles = StyleSheet.create({
    spotConttainer: {
        marginTop: 30,
        elevation: 12,
        borderRadius: 20,

    },
    imageContainer: {
        height: 250,
        borderRadius: 20,
        zIndex: 2,
    },
    top: {
        // backgroundColor: 'grey',
        margin: 20,
        // marginTop: 80,
        zIndex: 1,

    },
    container: {
        borderRadius: 25,
    },
    contentContainerStyle: {},
    item: {
        borderRadius: 10,
        width: SCREEN_WIDTH - 40,

    },
    iconContainerStyle: {
        zIndex: 10,
        alignItems: 'flex-end',
        marginRight: -20,
        marginBottom: -62,
    },
    iconStyle: {
        borderRadius: 40,
        borderWidth: 3,
        borderColor: 'white',
        marginTop: -10,
    },
    caption: {
        height: 250,
        width: SCREEN_WIDTH - 40,
        position: 'absolute',
        alignItems: 'center',
        justifyContent: 'center',
        zIndex: 5,
    },
    captionText: {
        color: 'black',
        fontSize: 25,
        backgroundColor: 'rgba(198,198,198,0.8)',
        width: '100%',
        textAlign: 'center',
    },
    descriptionText: {
        width: '100%',
        textAlign: 'center',
        backgroundColor: 'rgba(198,198,198,0.8)',
    },
//
// const styles = StyleSheet.create({
//     cardLayout:{
//         borderRadius:20,
//     },
//     textColor: {
//         color: 'white'
//     },
//     linearGradient: {
//         borderRadius:20,
//     },
//     container: {
//         backgroundColor: '#96e5ff',
//
//         flex: 1,
//     },
//     cardExtra: {
//         color: '#FFFFFF',
//         backgroundColor: '#6930C3',
//         borderRadius: 20
//     },
//
//     boxTop:{
//         margin: 20,
//         flexDirection: 'row',
//         justifyContent: 'space-evenly',
//     },
//     box: {
//         flexWrap: 'wrap',
//         margin: 20,
//         flexDirection: 'row',
//         justifyContent: 'space-evenly',
//     },
//     chartBox: {
//         margin: 20,
//         backgroundColor: '#4EA8DE',
//         borderRadius: 20
//     },
//     cardContent: {
//         color: '#FFFFFF',
//         justifyContent: 'center',
//         alignItems: 'center'
//
//     },
//     list: {
//         flex: 1,
//     },
});


export default connect(store => {
    return {...store};
})(MySpotScreen);


