



import { connect } from 'react-redux'
import React, {useEffect, useState} from 'react';
import {
    StatusBar,
    StyleSheet,
    Text,
    View,
    Image,
    TouchableOpacity, Dimensions, ScrollView, ActivityIndicator, SafeAreaView, FlatList, RefreshControl,
} from 'react-native';
import * as systemActions from '../../../store/actions/systemActions';
import {Surface} from 'react-native-paper';
import Icon from 'react-native-vector-icons/Ionicons';
import {useNavigation} from '@react-navigation/native';

const {width, height} = Dimensions.get('window');
const SCREEN_HEIGHT = height;
const SCREEN_WIDTH = width;
const ASPECT_RATIO = width / height;



const MySpotScreen = (props) => {
    const [page, setPage] = useState(1);
    const [refreshing, setRefreshing] = React.useState(false);
    const navigation = useNavigation();

    useEffect(() => {
        if (props.userReducer.user_data.user_spots.fetching === false && props.userReducer.user_data.user_spots.is_fetched === false) {
            props.dispatch(systemActions.fethUserSpots());
        }
    });

    const onRefresh = React.useCallback(() => {
        props.dispatch(systemActions.fethUserSpots());
    });

    function createSession(spot) {
        navigation.navigate('New Session', {spot: spot});
    }

    function goTodetail(spot) {
        navigation.navigate('Spot detail', {spot: spot});
    }

    const renderSpotItem = (data) => {
        let spot = data.item;


        return (
            <Surface key={'spot-' + spot.id} style={styles.top}>
                <View style={styles.iconContainerStyle}>
                    <TouchableOpacity
                        onPress={() => createSession(spot)}
                    >
                        <Icon
                            iconStyle={styles.iconStyle}
                            reverse
                            size={50}
                            name='add-circle'
                            color='#517fa4'
                        />
                    </TouchableOpacity>
                </View>
                <TouchableOpacity
                    onPress={() => goTodetail(spot)}
                >
                    <Image
                        style={styles.imageContainer}
                        source={{uri: 'https://www.wondersofmaui.com/wp-content/uploads/2015/09/hookipa-beach-empty.jpg'}}
                    />
                    <View style={styles.caption}>
                        <Text style={styles.captionText}>
                            {spot.name}
                        </Text>
                        <Text style={styles.descriptionText}>
                            0 active
                        </Text>
                    </View>
                </TouchableOpacity>
            </Surface>
        );
    };

    return (
        <FlatList
            refreshControl={
                <RefreshControl
                    refreshing={refreshing}
                    onRefresh={onRefresh}
                />
            }
            data={props.userReducer.user_data.user_spots.data}
            renderItem={renderSpotItem}
        />
    );
}

const styles = StyleSheet.create({
    title: {
        fontSize: 32,
    },
    spotConttainer: {
        marginTop: 30,
        elevation: 12,
        borderRadius: 20,

    },
    imageContainer: {
        height: 150,
        borderRadius: 20,
        zIndex: 2,
    },
    top: {
        // backgroundColor: 'grey',
        margin: 20,
        // marginTop: 80,
        zIndex: 1,

    },
    container: {
        borderRadius: 25,
    },
    contentContainerStyle: {},
    item: {
        borderRadius: 10,
        width: SCREEN_WIDTH - 40,

    },
    iconContainerStyle: {
        zIndex: 10,
        alignItems: 'flex-end',
        marginRight: -20,
        marginBottom: -62,
    },
    iconStyle: {
        borderRadius: 40,
        borderWidth: 3,
        borderColor: 'white',
        marginTop: -10,
    },
    caption: {
        height: 150,
        width: SCREEN_WIDTH - 40,
        position: 'absolute',
        alignItems: 'center',
        justifyContent: 'center',
        zIndex: 5,
    },
    captionText: {
        color: 'black',
        fontSize: 25,
        backgroundColor: 'rgba(198,198,198,0.8)',
        width: '100%',
        textAlign: 'center',
    },
    descriptionText: {
        width: '100%',
        textAlign: 'center',
        backgroundColor: 'rgba(198,198,198,0.8)',
    },
});

export default connect(store => {
    return{...store}
})(MySpotScreen);


