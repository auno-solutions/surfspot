import * as React from 'react';
import {Modal, Portal, Text, Button, Provider, Card} from 'react-native-paper';
import {useEffect} from 'react';

import {Dimensions, ScrollView, StyleSheet, View} from 'react-native';

import MapView, {
    Marker,
} from 'react-native-maps';

const {width: viewportWidth, height: viewportHeight} = Dimensions.get('window');


const MyComponent = (props) => {
    const [visible, setVisible] = React.useState(false);
    const [marker, setMarker] = React.useState(undefined);

    const {width, height} = Dimensions.get('window');
    const ASPECT_RATIO = width / height;
    const LATITUDE = 51.223177;
    const LONGITUDE = 2.893385;
    const LATITUDE_DELTA = 0.0922;
    const LONGITUDE_DELTA = LATITUDE_DELTA * ASPECT_RATIO;
    const SPACE = 0.01;


    useEffect(() => {
        if (props.selectedMarker !== undefined){
            setMarker(props.selectedMarker);
        }
    }, []);

    const showModal = () => setVisible(true);
    const hideModal = () => setVisible(false);
    const containerStyle = {
        backgroundColor: 'white', padding: 20, zIndex: 10,
    };




    return (
        <View>
            <Portal>
                <Modal visible={visible} onDismiss={hideModal} style={styles.modal}
                       contentContainerStyle={containerStyle}>
                    <MapView
                        style={styles.map}
                        initialRegion={{
                            latitude: LATITUDE,
                            longitude: LONGITUDE,
                            latitudeDelta: LATITUDE_DELTA,
                            longitudeDelta: LONGITUDE_DELTA,
                        }}
                        onPress={(e) =>{
                                setMarker({latlng: e.nativeEvent.coordinate});
                                props.updateItem({latlng: e.nativeEvent.coordinate});
                        }}
                    >
                        {marker !== undefined?
                            <Marker coordinate={marker.latlng}/>
                            :
                            null
                        }
                    </MapView>
                    <View style={styles.buttonContainer}>
                        <View style={styles.bubble}>
                            <Text>Tap on markers to see different callouts</Text>

                            {marker !== undefined?
                                <Text>{marker.latlng.latitude} - {marker.latlng.longitude}</Text>
                                :
                                null
                            }

                        </View>
                    </View>
                </Modal>
            </Portal>
            <Button icon="map-marker" mode="contained" onPress={showModal}>
                ADD LOCATION
            </Button>
        </View>
    );
};
const styles = StyleSheet.create({
    modal: {},
    map: {
        top: 10,
        height: viewportHeight - 250,
    },
    buttonContainer: {
        marginTop: -10,
        flexDirection: 'row',
        marginVertical: 20,
        backgroundColor: 'grey',
        bottom: 10,
        borderRadius: 20,

    },
    bubble: {
        flex: 1,
        backgroundColor: 'rgba(255,255,255,0.7)',
        paddingHorizontal: 18,
        paddingVertical: 12,
        borderRadius: 20,
    },
});


export default MyComponent;
