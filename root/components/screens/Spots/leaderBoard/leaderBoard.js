import * as React from 'react';
import {List, Title} from 'react-native-paper';
import {connect} from 'react-redux';

import * as SystemActions from '../../../../store/actions/systemActions'

import {
    StyleSheet,
    Text,
    View,
    Image,
    ScrollView,
    TouchableOpacity,
    FlatList
} from 'react-native';
import {useEffect} from 'react';

const data = [
    {id:1, image: "https://bootdey.com/img/Content/avatar/avatar6.png", username:"johndoe1",rank: 1,session_count:15},
    // {id:2, image: "https://bootdey.com/img/Content/avatar/avatar2.png", username:"johndoe2",rank: 2,sessionCount:14},
    // {id:3, image: "https://bootdey.com/img/Content/avatar/avatar3.png", username:"johndoe3",rank: 3,sessionCount:10},
    // {id:4, image: "https://bootdey.com/img/Content/avatar/avatar4.png", username:"johndoe4",rank: 4,sessionCount:8},
    // {id:5, image: "https://bootdey.com/img/Content/avatar/avatar1.png", username:"johndoe5",rank: 5,sessionCount:6},
];

const getRank = (number) => {
    switch (number) {
        case 0:
            return require('../../../../assets/award/rank1.png');
        case 1:
            return require('../../../../assets/award/rank2.png');
        case 2:
            return require('../../../../assets/award/rank3.png');
        default:
            return require('../../../../assets/award/otherrank.png');
    }
}


const LeaderBoard = (props) => {

    const [fetched, setIsFetched] = React.useState(false);
    const [data, setData] = React.useState([]);

    useEffect(() => {
        setIsFetched(false)
        fetchStats(props.spot.id);
    }, []);

    async function fetchStats(id) {
        let result = await SystemActions.fetchSpotStats(id);
        setIsFetched(true)
        setData(result);
    }

    return (
        <View style={styles.container}>
            <View style={styles.body}>
                {data.length > 0?
                    <FlatList
                        style={styles.container}
                        enableEmptySections={true}
                        data={data}
                        keyExtractor= {(item) => {
                            return item.id;
                        }}
                        renderItem={({item,index}) => {
                            return (
                                <View style={styles.box}>
                                    <Image style={styles.rank} source={getRank(index)}/>
                                    <Image style={styles.image} source={{src: item.image}}/>
                                    <Text style={styles.username}>{item.username}</Text>
                                    <View style={styles.iconContent}>
                                        <Text style={styles.sessionText}>{item.session_count}</Text>
                                    </View>
                                </View>
                            )
                        }}
                        ListHeaderComponent={()=>{
                            return(
                                <View>
                                    <View style={styles.box}>
                                        <Text style={styles.headerRank}>Rank</Text>
                                        <Text style={styles.headerUsername}>User</Text>
                                        <Text style={styles.headerIconContent}>sessions</Text>
                                    </View>
                                </View>
                            )
                        }}
                    />
                    :
                    <View style={styles.noData}>
                        <Text>There where no session found</Text>
                    </View>
                }
            </View>
        </View>
    );
}

const styles = StyleSheet.create({
    noData:{
      marginTop: 25,
    },
    container:{
        marginBottom:10
    },
    leadHeaderText: {
        width:400,
        height:140,
    },
    leadHeader:{
        marginTop:40,
        height:40,
        width:400,
        alignItems: 'center',
        justifyContent: 'center',
        elevation: 4,
        margin: 20,
        borderRadius: 20,
    },
    headerRank:{
        fontSize:15,
        alignSelf:'center',
    },
    headerUsername:{
        color: "#20B2AA",
        fontSize:15,
        alignSelf:'center',
        marginLeft:90,
    },
    headerIconContent:{
        color: "#20B2AA",
        fontSize:15,
        alignSelf:'center',
        marginLeft:85,
    },
    image:{
        width: 30,
        height: 30,
    },
    body: {
        flexDirection: 'row',
    },
    box: {
        width:300,
        backgroundColor: '#FFFFFF',
        flexDirection: 'row',
        shadowColor: 'black',
        shadowOpacity: .2,
        shadowOffset: {
            height:1,
            width:-2
        },
        elevation:2
    },
    rank:{
        height:50,
        width:40,
    },
    username:{
        color: "#20B2AA",
        fontSize:22,
        alignSelf:'center',
        marginLeft:25
    },
    iconContent:{
        width: 60,
        backgroundColor: '#40E0D0',
        marginLeft: 'auto',
        alignItems: 'center'
    },
    sessionText:{
        paddingTop:15,
        textAlign:'center',
        fontSize:15,
    },
    icon:{
        width: 40,
        height: 40,
    }
});

export default connect(store => {
    return {...store};
})(LeaderBoard);