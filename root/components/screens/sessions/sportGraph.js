import {connect} from 'react-redux';
import {Text, View, Dimensions} from 'react-native';
import React, {useEffect} from 'react';
import TimeLineComponent from '../../sessionTimeLine/timeLineComponent';

import {LineChart} from 'react-native-chart-kit';
import * as systemActions from '../../../store/actions/systemActions';

const SportGraph = (props) => {

    useEffect(() => {
        if (!props.userReducer.user_data.user_graphs.sport.is_fetched) {
            props.dispatch(systemActions.fethUserSportGraph());

        }
        // props.dispatch({
        //     type: 'TEST',
        //     payload: 'test'
        // });

    }, []);

    const screenWidth = Dimensions.get('window').width - 40;

    const chartConfig = {
        backgroundColor: '#4EA8DE',
        backgroundGradientFrom: '#4EA8DE',
        backgroundGradientTo: '#4EA8DE',
        decimalPlaces: 0, // optional, defaults to 2dp
        color: (opacity = 1) => `rgba(255, 255, 255, ${opacity})`,
        labelColor: (opacity = 1) => `rgba(255, 255, 255, ${opacity})`,
        style: {
            borderRadius: 16,
        },
        propsForDots: {
            r: '6',
            strokeWidth: '2',
            stroke: '#4EA8DE',
        },

    };

    const data = () => {

        props.userReducer.user_data.user_graphs.sport.data.datasets.map((item) => {
            console.log('-------------------');
            console.log(item)
            console.log('-------------------');
            if(item.sport === 'Windsurf'){
                item.color = (opacity = 1) => `rgba(134, 65, 244, ${opacity})`;
            }
            if(item.sport === 'Surf'){
                item.color= (opacity = 1) => `rgba(128, 255, 219, ${opacity})`;
            }

            if(item.sport === 'Surf'){
                item.color= (opacity = 1) => `rgba(128, 255, 219, ${opacity})`;
            }
            if(item.sport === 'Kitesurf'){
                item.color= (opacity = 1) => `rgba(231, 255, 48, ${opacity})`;
            }

        });

        // let test ={
        //     labels: ['January', 'February', 'March', 'April', 'May', 'June'],
        //     datasets: [
        //         {
        //             data: [3, 10, 0, 8, 15, 4],
        //             color: (opacity = 1) => `rgba(134, 65, 244, ${opacity})`, // optional
        //             strokeWidth: 2, // optional
        //         },
        //         {
        //             data: [10, 20, 3, 1, 2, 1],
        //             color: (opacity = 1) => `rgba(128, 255, 219, ${opacity})`, // optional
        //             strokeWidth: 2, // optional
        //         },
        //     ],
        //     legend: ['Windsurf','Surf'], // optional
        // };

        return props.userReducer.user_data.user_graphs.sport.data;
    };

    if (props.userReducer.user_data.user_graphs.sport.is_fetched && props.userReducer.user_data.user_graphs.sport.data.datasets.length !== 0) {
        return (
            <LineChart
                style={{
                    borderRadius: 20,
                }}
                data={data()}
                width={screenWidth}
                height={220}
                chartConfig={chartConfig}
                bezier
            />

        );
    } else {
        return(
            <Text>No data</Text>
        );
    }
};

export default connect(store => {
    return {...store};
})(SportGraph);


