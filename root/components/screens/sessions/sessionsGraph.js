import {connect} from 'react-redux';
import {Text, View, Dimensions} from 'react-native';
import React, {useEffect} from 'react';
import TimeLineComponent from '../../sessionTimeLine/timeLineComponent';
import * as systemActions from '../../../store/actions/systemActions';


import {LineChart} from 'react-native-chart-kit';

const SessionsGraph = (props) => {

    useEffect(() => {

        if (!props.userReducer.user_data.user_graphs.sessions.is_fetched) {
            props.dispatch(systemActions.fethUserSessionsGraph());
        }
        // props.dispatch({
        //     type: 'TEST',
        //     payload: 'test'
        // });

    }, []);

    const screenWidth = Dimensions.get('window').width - 40;

    const chartConfig = {
        backgroundColor: '#e26a00',
        backgroundGradientFrom: '#fb8c00',
        backgroundGradientTo: '#ffa726',
        decimalPlaces: 0, // optional, defaults to 2dp
        color: (opacity = 1) => `rgba(255, 255, 255, ${opacity})`,
        labelColor: (opacity = 1) => `rgba(255, 255, 255, ${opacity})`,
        style: {
            borderRadius: 16,
        },
        propsForDots: {
            r: '6',
            strokeWidth: '2',
            stroke: '#ffa726',
        },

    };


    const data = () => {
        return {
            labels: props.userReducer.user_data.user_graphs.sessions.data.labels,
            datasets: [
                {
                    data: props.userReducer.user_data.user_graphs.sessions.data.datasets [0].data,
                    color: (opacity = 1) => props.userReducer.user_data.user_graphs.sessions.data.datasets [0].color, // optional
                    strokeWidth: props.userReducer.user_data.user_graphs.sessions.data.datasets [0].strokeWidth, // optional
                },
            ],
            legend: [props.userReducer.user_data.user_graphs.sessions.data.legend], // optional
        };
    };
    if (props.userReducer.user_data.user_graphs.sessions.is_fetched && props.userReducer.user_data.user_graphs.sessions.data.datasets.length !== 0) {
        return (
            <LineChart
                style={{
                    borderRadius: 20,
                }}
                data={data()}
                width={screenWidth}
                height={220}
                chartConfig={chartConfig}
                bezier
            />
        );

    } else {
        return(
            <Text>No data</Text>
        );
    }


};

export default connect(store => {
    return {...store};
})(SessionsGraph);


