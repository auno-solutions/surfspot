import {connect} from 'react-redux';
import React, {useEffect} from 'react';
import {TouchableOpacity} from 'react-native';
import {
    StyleSheet,
    Text,
    View,
    Image,
    ScrollView,
} from 'react-native';


import {
    Dropdown,
    GroupDropdown,
    MultiselectDropdown,
} from 'sharingan-rn-modal-dropdown';
import {Card, Subheading} from 'react-native-paper';
import * as userActions from '../../../store/actions/userActions';

export const groupData = [
    {
        value: '99',
        label: 'iPhone 12 Mini',
    },
];

const GearSelecitonScreen = (props) => {

        const [valueSS, setValueSS] = React.useState('');
        const [valueGS, setValueGS] = React.useState('');
        const [errorBox, setErrorBox] = React.useState(false);
        const [errorBoxMessage, setErrorBoxMessage] = React.useState('No sport selected');
        const [boards, setBoards] = React.useState([]);
        const [kites, setKites] = React.useState([]);
        const [sails, setSails] = React.useState([]);

        const [sport, setSport] = React.useState({id:1});


        const onChangeSS = (value: string) => {
            setValueSS(value);
        };
        const onChangeGS = (value: string) => {
            setValueGS(value);
        };


        useEffect(() => {

            if (props.userReducer.user_data.user_gear.is_fetched === false && props.userReducer.user_data.user_gear.fetching === false) {
                props.dispatch(userActions.fetchUserGear());
            }

            setErrorBoxMessage('No sport selected');
            let anySport = true;

            props.sportMap.map((item) => {
                if (item.selected) {
                    setSport(item);
                    let gear = props.userReducer.user_data.user_gear.data[item.id];
                    anySport = false;
                    if (gear === undefined) {
                        anySport = true;
                        setErrorBoxMessage('You have no gear for this sport');
                    } else {
                        setErrorBoxMessage('No sport selected');
                    }
                    if (!anySport) {
                        createGearDropdowns(gear);
                    }
                }
            });

            setErrorBox(anySport);


        }, [props.sportMap]);

        function createGearDropdowns(gear) {
            console.log('create gear select');
            console.log(gear);
            if (gear['Board']) {
                let temp = [];
                gear['Board'].map((boardItem) => {
                    temp.push(createDataObject(boardItem));
                });
                setBoards(temp);
            }
            if (gear['Sail']) {
                let temp = [];
                gear['Sail'].map((boardItem) => {
                    temp.push(createDataObject(boardItem));
                });
                setSails(temp);
            }

            if (gear['Kite']) {
                let temp = [];
                gear['Kite'].map((boardItem) => {
                    temp.push(createDataObject(boardItem));
                });
                setKites(temp);
            }
        }

        function createDataObject(boardItem){
            return {
                value: boardItem.id,
                label: boardItem.brand + ' ' + boardItem.typeValue + ' ' + boardItem.value,
            };
        }


        return (
            <View>
                {errorBox === true ?
                    <Text>{errorBoxMessage}</Text>
                    :
                    <View>
                        <View>
                            <Subheading>Board</Subheading>
                            <View style={styles.container}>
                                <Dropdown
                                    label="Select Board"
                                    data={boards}
                                    enableSearch
                                    value={valueGS}
                                    onChange={onChangeGS}
                                />
                            </View>
                        </View>
                        <View style={styles.formDivider}/>
                        {sport.id === 3 ?
                            <View>
                                <Subheading>kites</Subheading>
                                <View style={styles.container}>
                                    <Dropdown
                                        label="Select kite"
                                        data={kites}
                                        enableSearch
                                        value={valueGS}
                                        onChange={onChangeGS}
                                    />
                                </View>
                            </View>
                            :
                            <View>
                                <Subheading>Sail</Subheading>
                                <View style={styles.container}>
                                    <Dropdown
                                        label="Select sail"
                                        data={sails}
                                        enableSearch
                                        value={valueGS}
                                        onChange={onChangeGS}
                                    />
                                </View>
                            </View>
                        }

                    </View>
                }
            </View>
        )
            ;
    }
;
const styles = StyleSheet.create({
    container: {
        marginLeft: 20,
        marginRight: 20,
        flex: 1,
    },
    formDivider: {
        height: 20,
    },
});


export default connect(store => {
    return {...store};
})(GearSelecitonScreen);


