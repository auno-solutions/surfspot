



import { connect } from 'react-redux'
import {ScrollView, StyleSheet, Text, View} from 'react-native';
import React, { useEffect } from 'react';
import {Button, Card, Chip, Colors, Subheading, Surface, TextInput, ToggleButton} from 'react-native-paper';
import MapModal from './mapModal';
import ModalSelector from 'react-native-modal-selector';
import * as userActions from '../../../store/actions/userActions';
import Moment from 'moment';


const newSession = (props) => {

    const [loading, setLoading] = React.useState(false);
    const [showSportGearTypes, setShowSportGearTypes] = React.useState(false);
    const [sportSelectList, setSportSelectList] = React.useState([]);
    const [sportGeartTypes, setSportGeartTypes] = React.useState([]);
    const [selectedSportLabel, setSelectedSportLabel] = React.useState('');
    const [windValue, setWindValue] = React.useState('low');
    const [waveValue, setWaveValue] = React.useState('s');
    const [selectedGear, setSelectedGear] = React.useState({});

    const [newSession, setNewSession] = React.useState({
        spot: "/api/spots/"+props.route.params.spot.id,
        user: "/api/users/"+props.authReducer.user.data.id,
        description: '',
        sport: "/api/sports/",
        seesionStart: Moment().toISOString(),
    });


    useEffect(() => {

        if (props.userReducer.user_data.user_gear.is_fetched === false && props.userReducer.user_data.user_gear.fetching === false) {
            props.dispatch(userActions.fetchUserGearNormaLized());
        }

        if (props.userReducer.user_data.sports.is_fetched === false && props.userReducer.user_data.sports.fetching === false) {
            props.dispatch(userActions.fetchSports());
        }


        sportSelectListCreator();
    },[props.userReducer.user_data.sports.data,props.userReducer.user_data.user_gear.data]);


    function createSportGearType() {
        let dataList = [];
        let index = 0;

        // console.log(selectedSportItem.sportGearType)

        // selectedSportItem.sportGearType.map((sportType) => {
        //     dataList.push({key: index, section: false, label: sportType, data: sportType});
        //     index++;
        // });

        return dataList;
    }


    const sportSelectListCreator = () => {
        setLoading(true);
        let dataList = [];
        props.userReducer.user_data.sports.data.map((sportItem) => {
            dataList.push({key: sportItem.id, section: false, label: sportItem.sportName, data: sportItem});
        });

        console.log(dataList)

        setSportSelectList(dataList);

        setLoading(false);
    };


    function getUserGearByType(gearTypeName) {
        let dataList = [];

        props.userReducer.user_data.user_gear.data.map((gear) => {
            if(gear.type === gearTypeName && selectedSportLabel  === gear.sport.sportName ){
                dataList.push({key: gear.id, section: false, label: gear.value + '-'+ gear.type_value+'-'+ gear.brand, data: gear});
            }
        })

        return dataList;
    }

    function createUserSession() {
        // let currentDate = Moment().toISOString();
        //
        // let session = {
        //     spot: "/api/spots/"+props.route.params.spot.id,
        //     user: "/api/users/"+props.authReducer.user.data.id,
        //     description: text,
        //     sport: "/api/sports/"+selectedSport.id,
        //     seesionStart: currentDate,
        //
        // }
        //
        // props.dispatch(userActions.createNewSession(session));
        //
        // navigation.navigate('Home');
    }


    return (
        <ScrollView style={styles.container}>
            <View style={styles.formContainer}>
                <Surface style={styles.surface}>
                    <Card.Title title="Selected spot"/>
                    <MapModal spot={props.route.params.spot}/>
                    <Card.Title style={styles.cardSubTitle} title="" subtitle="Click the button to show the location"/>
                </Surface>

            </View>

            <View style={styles.formContainer}>

                <Card>
                    <Card.Content>
                        <ModalSelector
                            data={sportSelectList}
                            initValue="Select sport"
                            supportedOrientations={['landscape']}
                            accessible={true}
                            scrollViewAccessibilityLabel={'Scrollable options'}
                            cancelButtonAccessibilityLabel={'Cancel Button'}
                            animationType="fade"
                            onChange={(option) => {
                                setSelectedSportLabel(option.label)
                                setNewSession({
                                    ...newSession,
                                    sport: "/api/sports/"+option.data.id,
                                })
                                setSportGeartTypes(option.data.sportGearType)

                                setShowSportGearTypes(true)
                            }}>

                            <TextInput
                                style={{borderWidth: 1, borderColor: '#ffffff', padding: 10, height: 25, color: Colors.white, backgroundColor: Colors.white,textColor: Colors.white}}
                                editable={false}
                                placeholder="Select sport"
                                value={selectedSportLabel}/>

                        </ModalSelector>
                    </Card.Content>
                    <View style={styles.formDivider}/>
                </Card>
            </View>
            {showSportGearTypes &&
            <View style={styles.formContainer}>
                <Card>
                    <Card.Content>
                        {sportGeartTypes.map((gearTypeName)=>{

                            return(
                                <ModalSelector
                                    key={'select'+gearTypeName}
                                    data={getUserGearByType(gearTypeName)}
                                    initValue="Select"
                                    supportedOrientations={['landscape']}
                                    accessible={true}
                                    scrollViewAccessibilityLabel={'Scrollable options'}
                                    cancelButtonAccessibilityLabel={'Cancel Button'}
                                    onChange={(option) => {
                                        setSelectedGear({
                                            ...selectedGear,
                                            [gearTypeName] : option.data.value + '-'+ option.data.type_value+'-'+ option.data.brand
                                        })

                                    }}>

                                    <TextInput
                                        key={'select'+gearTypeName+'name'}
                                        style={{borderWidth: 1, borderColor: '#ffffff', padding: 10, height: 40, color: 'black', backgroundColor: '#fdfdfd'}}
                                        editable={false}
                                        placeholder={'Select ' + gearTypeName}
                                        // value={()=> { (selectedGear.filter(data => data.type === gearTypeName).type)}}
                                        value={selectedGear[gearTypeName]}

                                    />
                                </ModalSelector>

                            )
                        })}

                    </Card.Content>
                    <View style={styles.formDivider}/>
                </Card>

                <Card>
                    <Card.Title title="Session Conditions"/>
                    <Card.Content>
                        <Subheading>Wind</Subheading>
                        <ToggleButton.Row onValueChange={value => setWindValue(value)} value={windValue}>
                            <ToggleButton icon="gauge-empty" value="low" />
                            <ToggleButton icon="gauge-low" value="slow" />
                            <ToggleButton icon="gauge" value="medium" />
                            <ToggleButton icon="gauge-full" value="fast" />
                            <ToggleButton icon="nuke" value="nuke" />
                        </ToggleButton.Row>
                        <View style={styles.formDivider}/>
                        <Subheading>Waves</Subheading>
                        <ToggleButton.Row onValueChange={value => setWaveValue(value)} value={waveValue}>
                            <ToggleButton icon="wave" value="flat" />
                            <ToggleButton icon="size-s" value="s" />
                            <ToggleButton icon="size-m" value="m" />
                            <ToggleButton icon="size-l" value="l" />
                            <ToggleButton icon="size-xl" value="xl" />
                        </ToggleButton.Row>
                    </Card.Content>
                    <View style={styles.formDivider}/>
                    <Card.Content>
                        <Subheading>Tell about your session</Subheading>
                        <TextInput
                            style={styles.inputStyle}
                            mode='outlined'
                            label="Description"
                            value={''}
                        />
                    </Card.Content>
                </Card>

                <View style={styles.formDivider}/>
                <View style={styles.buttonGroup}>
                    <Button icon="check" mode="contained" onPress={() => createUserSession()}>
                        Create
                    </Button>
                </View>

            </View>
            }


        </ScrollView>
    );
}


const styles = StyleSheet.create({
    textColor: {
        color: 'white',
    },
    container: {
        backgroundColor: '#f8f8f8',
        flex: 1,
        zIndex:1,
    },
    formContainer: {
        margin: 20,
    },
    formDivider: {
        height: 20,
    },
    inputStyle: {
        marginTop: 10,
        backgroundColor: '#ffffff',

    },
    cardSubTitle: {
        marginTop: -25,
        marginBottom: -20,

    },
    chipStyle: {
        marginTop: 15,
    },
    buttonGroup: {
        justifyContent: 'space-evenly',
    },
    surface: {
        alignItems: 'center',
        justifyContent: 'center',
        elevation: 2,
    },

});

export default connect(store => {
    return{...store}
})(newSession);


