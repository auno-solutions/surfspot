import {connect} from 'react-redux';
import {ScrollView, StyleSheet, Text, View} from 'react-native';
import React, {useEffect} from 'react';
import * as systemActions from '../../../store/actions/systemActions';

import {
    TextInput,
    Button,
    Title,
    Subheading,
    Card,
    Chip,
    ToggleButton,
    Portal,
    Surface,
    Checkbox,
} from 'react-native-paper';
import * as userActions from '../../../store/actions/userActions';
import MapModal from  './mapModal';
import Moment from 'moment';
import GearSelectionScreen from './sessionGearSelection'
import {useNavigation} from '@react-navigation/native';



const AddSession = (props) => {
    const [text, setText] = React.useState('Added from app');
    const [selectedSport, setSelectedSport] = React.useState({id: 1 ,name: 'Windsurf', selected: true});

    const [sports, setSports] = React.useState([
        {id: 1 ,name: 'Windsurf', selected: true},
        {id: 2 ,name: 'Surf', selected: false},
        {id: 3 ,name: 'Kitesurf', selected: false},
    ]);

    const [windValue, setWindValue] = React.useState('low');
    const [waveValue, setWaveValue] = React.useState('s');
    const [checked, setChecked] = React.useState(false);
    const navigation = useNavigation();


    useEffect(() => {


    });

    function updateChips(updateItem) {
        let elementsIndex = sports.findIndex(element => element.name === updateItem.name);
        let newArray = [...sports];
        newArray[elementsIndex] = {...newArray[elementsIndex], selected: !newArray[elementsIndex].selected};

        newArray.filter((item) => {
            if (newArray[elementsIndex] !== item) {
                item.selected = false;
            }
        });
        setSelectedSport(newArray[elementsIndex]);
        setSports(newArray);
        console.log('update')
    }


    function createUserSession() {
        let currentDate = Moment().toISOString();

        let session = {
            spot: "/api/spots/"+props.route.params.spot.id,
            user: "/api/users/"+props.authReducer.user.data.id,
            description: text,
            sport: "/api/sports/"+selectedSport.id,
            seesionStart: currentDate,

        }

        props.dispatch(userActions.createNewSession(session));

        navigation.navigate('Home');
    }
    const handleUpdate = () => {
        console.log('ici');
    }

    return (

        <ScrollView style={styles.container}>


            <View style={styles.formContainer}>
                <Surface style={styles.surface}>
                    <Card.Title title="Selected spot"/>
                    <MapModal spot={props.route.params.spot}/>
                    <Card.Title style={styles.cardSubTitle} title="" subtitle="Click the button to show the location"/>
                </Surface>
                {/*<Card>*/}
                {/*    <Card.Title title="Selected spot"/>*/}
                {/*    <Card.Content>*/}
                {/*    </Card.Content>*/}
                {/*</Card>*/}
                <View style={styles.formDivider}/>

                <Card>
                    <Card.Title title="Sport"/>
                    <Card.Content>


                        {sports.map((item) => {
                            return (
                                <Chip style={styles.chipStyle} selected={item.selected} icon="information"
                                      onPress={() => updateChips(item)}>{item.name}</Chip>
                            );
                        })
                        }



                    </Card.Content>
                    <View style={styles.formDivider}/>
                </Card>
                <View style={styles.formDivider}/>
                <Card>
                    <Card.Title title="Session Gear"/>
                    <Card.Content>
                        <GearSelectionScreen sportMap={sports} handleUpdate={()=> handleUpdate}/>
                    </Card.Content>
                </Card>

                <View style={styles.formDivider}/>
                <Card>
                    <Card.Title title="Session Conditions"/>
                    <Card.Content>
                            <Subheading>Wind</Subheading>
                            <ToggleButton.Row onValueChange={value => setWindValue(value)} value={windValue}>
                                <ToggleButton icon="gauge-empty" value="low" />
                                <ToggleButton icon="gauge-low" value="slow" />
                                <ToggleButton icon="gauge" value="medium" />
                                <ToggleButton icon="gauge-full" value="fast" />
                                <ToggleButton icon="nuke" value="nuke" />
                            </ToggleButton.Row>
                            <View style={styles.formDivider}/>
                        <Subheading>Waves</Subheading>
                        <ToggleButton.Row onValueChange={value => setWaveValue(value)} value={waveValue}>
                            <ToggleButton icon="wave" value="flat" />
                            <ToggleButton icon="size-s" value="s" />
                            <ToggleButton icon="size-m" value="m" />
                            <ToggleButton icon="size-l" value="l" />
                            <ToggleButton icon="size-xl" value="xl" />
                        </ToggleButton.Row>
                    </Card.Content>
                    <View style={styles.formDivider}/>
                    <Card.Content>
                        <Subheading>Tell about your session</Subheading>
                        <TextInput
                            style={styles.inputStyle}
                            mode='outlined'
                            label="Description"
                            value={text}
                            onChangeText={text => setText(text)}
                        />
                    </Card.Content>
                </Card>


                <View style={styles.formDivider}/>
                <View style={styles.buttonGroup}>
                    <Button icon="check" mode="contained" onPress={() => createUserSession()}>
                        Create
                    </Button>
                </View>
            </View>
        </ScrollView>

    );
};

const styles = StyleSheet.create({
    textColor: {
        color: 'white',
    },
    container: {
        backgroundColor: '#f8f8f8',
        flex: 1,
        zIndex:1,
    },
    formContainer: {
        margin: 20,
    },
    formDivider: {
        height: 20,
    },
    inputStyle: {
        marginTop: 10,
        backgroundColor: '#ffffff',

    },
    cardSubTitle: {
        marginTop: -25,
        marginBottom: -20,

    },
    chipStyle: {
        marginTop: 15,
    },
    buttonGroup: {
        justifyContent: 'space-evenly',
    },
    surface: {
        alignItems: 'center',
        justifyContent: 'center',
        elevation: 2,
    },

});

export default connect(store => {
    return {...store};
})(AddSession);


