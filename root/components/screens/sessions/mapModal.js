import * as React from 'react';
import {Modal, Portal, Text, Button, Provider, Card} from 'react-native-paper';
import {useEffect} from 'react';

import {Dimensions, ScrollView, StyleSheet, View} from 'react-native';

import MapView, {
    Marker,
} from 'react-native-maps';

const {width: viewportWidth, height: viewportHeight} = Dimensions.get('window');


const MyComponent = (props) => {
    const [visible, setVisible] = React.useState(false);

    const {width, height} = Dimensions.get('window');
    const ASPECT_RATIO = width / height;
    const LATITUDE = 51.223177;
    const LONGITUDE = 2.893385;
    const LATITUDE_DELTA = 0.0922;
    const LONGITUDE_DELTA = LATITUDE_DELTA * ASPECT_RATIO;
    const SPACE = 0.01;


    useEffect(() => {
        props.spot.latitude = props.spot.latitude === undefined? props.spot.lattitude: props.spot.latitude;
    }, []);

    const showModal = () => setVisible(true);
    const hideModal = () => setVisible(false);
    const containerStyle = {backgroundColor: 'white', padding: 20,zIndex:10,
    };

    return (
        <View>
            <Portal>
                <Modal visible={visible} onDismiss={hideModal} style={styles.modal} contentContainerStyle={containerStyle}>
                    <MapView
                        style={styles.map}
                        initialRegion={{
                            latitude: parseFloat(props.spot.latitude),
                            longitude: parseFloat(props.spot.longitude),
                            latitudeDelta: LATITUDE_DELTA,
                            longitudeDelta: LONGITUDE_DELTA,
                        }}
                    >
                        <Marker
                            key={'1'}
                            coordinate={{
                                latitude: parseFloat(props.spot.latitude),
                                longitude: parseFloat(props.spot.longitude),
                            }}
                            calloutVisible={true}
                            title={props.spot.name}
                        />
                    </MapView>

                </Modal>
            </Portal>
            <Button icon="map-marker" mode="contained" onPress={showModal}>
                {props.spot.name}
            </Button>
        </View>
    );
};
const styles = StyleSheet.create({
    map: {
        height: 500,
    },
});


export default MyComponent;
