import {connect} from 'react-redux';
import React, {useEffect} from 'react';
import {TouchableOpacity} from 'react-native';
import {Button, List, Switch, ToggleButton} from 'react-native-paper';

import {
    StyleSheet,
    Text,
    View,
    Image,
    ScrollView,
} from 'react-native';
import MultiSlider from '@ptomasroos/react-native-multi-slider';

const SettingsScreen = (props) => {

        const [selectedTab, setSelectedTab] = React.useState(1);


        const [notificationsOn, setNotificationsOn] = React.useState(true);
        const [publicProfile, setPublicProfile] = React.useState(true);
        const [range, setrange] = React.useState(50);


        useEffect(() => {

        }, []);

        const publicProfileToggleSwitch = () => setPublicProfile(!publicProfile);

        return (
            <View>
                <List.Item
                    title="Public account"
                    description="Other people can see you profile"
                    left={props => <List.Icon {...props} icon="account"/>}
                    right={props =>
                        <Switch
                            value={publicProfile}
                            onValueChange={publicProfileToggleSwitch}
                        />
                    }
                />
                <List.Item
                    title="Allow notifications"
                    description="Send message when a friend post a new session"
                    left={props => <List.Icon {...props} icon="network-outline"/>}
                    right={props =>
                        <Switch
                            value={notificationsOn}
                            onValueChange={setNotificationsOn}
                        />
                    }
                />
                <List.Item
                    title="Spot finder"
                    description="Range circkel from your location"
                    left={props => <List.Icon {...props} icon="network-outline"/>}
                    right={(props, style) => {
                        return (
                            <Text style={styles.sliderValueText}>{range}</Text>
                        );
                    }

                    }
                />
                <View style={styles.sliderView}>
                    <MultiSlider
                        values={[range]}
                        min={10}
                        max={100}
                        onValuesChange={(val) => {
                            setrange(val);
                        }}
                    />
                </View>
                <View style={styles.logout}>
                    <Button icon="logout" mode="contained" onPress={() => {

                        props.dispatch({
                            type: 'DEV_LOGOUT',
                            payload: true
                        });

                    }}>
                        Logout
                    </Button>
                </View>

            </View>
        );
    }
;
const styles = StyleSheet.create({
    logout:{
        margin:20

    },
    sliderValueText: {
        marginTop: 15,
        marginRight: 25,
        fontSize: 15,

    },
    sliderView: {
        marginLeft: 50,
        marginTop: -20,
        justifyContent: 'center',
    },
});


export default connect(store => {
    return {...store};
})(SettingsScreen);


