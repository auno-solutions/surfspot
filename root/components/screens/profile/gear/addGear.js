import {connect} from 'react-redux';
import {Switch, Text, View} from 'react-native';
import React, {useEffect} from 'react';
import ModalSelector from 'react-native-modal-selector';
import {ActivityIndicator, Button, Colors, TextInput} from 'react-native-paper';
import * as userActions from '../../../../store/actions/userActions';
import {useNavigation} from '@react-navigation/native';


const addNewGear = (props) => {
    const navigation = useNavigation();

    const [loading, setLoading] = React.useState(false);

    const [selectedSportItem, setSelectedSportItem] = React.useState({});
    const [selectedSportLabel, setSelectedSportLabel] = React.useState('');
    const [selectedSportGearLabel, setSelectedSportGearLabel] = React.useState('');
    const [showSportGearTypes, setShowSportGearTypes] = React.useState(false);
    const [showSportGearTypesOption, setShowSportGearTypesOption] = React.useState(false);
    const [sportSelectList, setSportSelectList] = React.useState([]);
    const [newGearitem, setNewGearitem] = React.useState({
        brand: '...',
        type_value: '...',
        size: '..',
        sport_id: null,
        type: null,
        sportName: null,
        user_id: null
    });


    useEffect(() => {
        sportSelectListCreator();
        // navigation.navigate('Profile',{snackbar: {text:'added gear',type:'succes' }});

    }, []);


    const sportSelectListCreator = () => {
        setLoading(true);
        let dataList = [];
        props.userReducer.user_data.sports.data.map((sportItem) => {
            dataList.push({key: sportItem.id, section: false, label: sportItem.sportName, data: sportItem});
        });
        setSportSelectList(dataList);

        setLoading(false);
    };


    function createSportGearType() {
        let dataList = [];
        let index = 0;

        selectedSportItem.sportGearType.map((sportType) => {
            dataList.push({key: index, section: false, label: sportType, data: sportType});
            index++;
        });

        return dataList;
    }

    function postNewGear() {
        setLoading(true);
        props.dispatch(userActions.addNewGear(newGearitem,props.authReducer.user.data.id));
        setLoading(false);
        navigation.navigate('Profile',{snackbar: {text:'added gear',type:'succes' }});

    }

    if(loading){

        return (
            <View style={{flex: 1,justifyContent:'space-around', paddingTop: 50}}>
                <ActivityIndicator animating={true} color={Colors.blue500} size='large'/>
            </View>
            )
    }

    return (
        <View style={{flex: 1, paddingTop: 50}}>

            <ModalSelector
                data={sportSelectList}
                initValue="Select sport"
                supportedOrientations={['landscape']}
                accessible={true}
                scrollViewAccessibilityLabel={'Scrollable options'}
                cancelButtonAccessibilityLabel={'Cancel Button'}
                onChange={(option) => {
                    setSelectedSportLabel(option.label);
                    setSelectedSportItem(option.data);
                    setShowSportGearTypes(true);
                    setShowSportGearTypesOption(false);
                    setSelectedSportGearLabel('Select '+(option.data.sportName).toLowerCase()+' gear')
                    setNewGearitem(
                        {
                            ...newGearitem,
                            sportName: option.data.sportName,
                            sport_id: option.data.id
                        }
                    )
                }}>

                <TextInput
                    style={{borderWidth: 1, borderColor: '#ffffff', padding: 10, height: 40, color: 'black', backgroundColor: '#fdfdfd'}}
                    editable={false}
                    placeholder="Select sport"
                    value={selectedSportLabel}/>

            </ModalSelector>


            {showSportGearTypes &&
            <View>
                <ModalSelector
                    data={createSportGearType()}
                    initValue="Select sport gear"
                    supportedOrientations={['landscape']}
                    accessible={true}
                    scrollViewAccessibilityLabel={'Scrollable options'}
                    cancelButtonAccessibilityLabel={'Cancel Button'}
                    onChange={(option) => {
                        setSelectedSportGearLabel(option.label);
                        setShowSportGearTypesOption(true);


                        setNewGearitem(
                            {
                                ...newGearitem,
                                type: option.label
                            }
                        )

                    }}>

                    <TextInput
                        style={{borderWidth: 1, borderColor: '#ffffff', padding: 10, height: 40, color: 'black', backgroundColor: '#fdfdfd'}}
                        editable={false}
                        placeholder="Select sport gear"
                        value={selectedSportGearLabel}/>

                </ModalSelector>
            </View>
            }
            {showSportGearTypes && showSportGearTypesOption &&
            <View>
                <TextInput
                    label={selectedSportGearLabel + ' brand name'}
                    value={newGearitem.brand}
                    // onChangeText={text => setText(text)}
                    onChangeText={text => setNewGearitem(
                        {
                            ...newGearitem,
                            brand: text
                        }
                    )}
                />

                <TextInput
                    label={selectedSportGearLabel + ' type '}
                    value={newGearitem.type_value}
                    onChangeText={text => setNewGearitem(
                        {
                            ...newGearitem,
                            type_value: text
                        }
                    )}
                />


                <TextInput
                    label={selectedSportGearLabel + ' size '}
                    value={newGearitem.size}
                    onChangeText={text => setNewGearitem(
                        {
                            ...newGearitem,
                            size: text
                        }
                    )}
                />
                <Button style={{marginTop:25}} mode="contained" onPress={() => postNewGear()}>add gear</Button>
            </View>
            }



       </View>
    );
};

export default connect(store => {
    return {...store};
})(addNewGear);


