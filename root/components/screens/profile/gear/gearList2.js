
import { connect } from 'react-redux'
import {Image, RefreshControl, ScrollView, Text, View} from 'react-native';
import React, {useEffect, useState} from 'react';
import * as userActions from '../../../../store/actions/userActions';
import {Avatar, Button, Colors, Divider, List} from 'react-native-paper';
import {black} from 'react-native-paper/src/styles/colors';
import {useNavigation} from '@react-navigation/native';

const GearList2 = (props) => {
    const navigation = useNavigation();
    const [refreshing, setRefreshing] = React.useState(false);


    useEffect(() => {

        if (props.userReducer.user_data.user_gear.is_fetched === false && props.userReducer.user_data.user_gear.fetching === false) {
            props.dispatch(userActions.fetchUserGearNormaLized());
        }

        if (props.userReducer.user_data.sports.is_fetched === false && props.userReducer.user_data.sports.fetching === false) {
            props.dispatch(userActions.fetchSports());
        }
    },[props.userReducer.user_data.user_gear.data]);

    function getsportIcon(sportName) {
        switch (sportName) {
            case 'surf_color.png':
                return require('../../../../assets/sport/surf_color.png');
            case 'kitesurf_color.png':
                return require('../../../../assets/sport/kitesurf_color.png');
            case 'windsurfing_color.png':
                return require('../../../../assets/sport/windsurfing_color.png');
            case 'wing_black.png':
                return require('../../../../assets/sport/wing_black.png');
            default:
                return require('../../../../assets/sport/surf_color.png');
        }
        // const LeftContentImage = props => <Image style={styles.tinyLogo} source={getsportIcon(session.sport.sportIcon)}/>

    }

    const onRefresh = React.useCallback(() => {
        props.dispatch(userActions.fetchSports());
    });

    return (
        <ScrollView
            refreshControl={
                <RefreshControl
                    refreshing={refreshing}
                    onRefresh={onRefresh}
                />
            }

        >
            { props.userReducer.user_data.sports.is_fetched === true &&
                    <List.Section>
                        {props.userReducer.user_data.sports.data.map((sportItem, index) => {
                            return (
                                <List.Accordion
                                    key={sportItem.id}
                                    style={{
                                        borderColor: Colors.grey300,
                                        borderTopWidth:1,
                                        height: 55,
                                        marginTop: 0,
                                        padding: 0
                                    }}
                                    title={sportItem.sportName}
                                    // left={props => <List.Icon {...props} color={Colors.black} icon="equal" />}
                                    // left={props=> <Avatar.Image size={40} source={getsportIcon(sportItem.sportIcon)} /> }
                                    left={props=> <Image style={{
                                        borderColor: black,
                                        width: 50,
                                        height: 50,
                                        marginRight:25
                                    }} source={getsportIcon(sportItem.sportIcon)}/> }
                                >


                                    {props.userReducer.user_data.user_gear.data.map((gearItem, index) => {
                                        if(gearItem.sport.id === sportItem.id){
                                            if(gearItem.user_default){
                                                return(
                                                    <List.Item key={gearItem.id} title={gearItem.value +'-'+ gearItem.type_value+'-'+ gearItem.brand} left={props => <List.Icon {...props} color={Colors.yellow800} icon="star" />} />
                                                )
                                            }
                                        }




                                    })}

                                    <Divider />
                                    <List.Item key={sportItem.id+'tot'} title={'0 items (add items count??)'} />
                                    {/*<List.Item title="All gear" style={{*/}
                                    {/*    backgroundColor:Colors.grey100,*/}
                                    {/*}}*/}
                                    {/*onPress={()=>{*/}
                                    {/*    alert('clicked')*/}
                                    {/*}}/>*/}

                                    <Button mode="outlined"
                                        icon="playlist-edit"
                                        style={{
                                            paddingLeft: -0
                                        }}
                                    onPress={()=>{
                                        navigation.navigate('Sport detail',{sport: sportItem});
                                    }}

                                    >Manage</Button>
                                </List.Accordion>
                        )
                        })}

                    </List.Section>
            }
            <Button mode="outlined"
                    onPress={()=>{
                        navigation.navigate('New gear');
                    }}
            >New gear</Button>

        </ScrollView>

    );
}

export default connect(store => {
    return{...store}
})(GearList2);


