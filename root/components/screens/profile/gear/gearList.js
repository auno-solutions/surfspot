import {connect} from 'react-redux';
import React, {useEffect, useState} from 'react';
import {
    StyleSheet,
    Text,
    View,
    Image,
    ScrollView, RefreshControl,
} from 'react-native';
import {
    Avatar,
    List,
    Divider,
    Subheading,
    Title,
    Button,
    IconButton,
    ActivityIndicator,
    Colors,
} from 'react-native-paper';
import * as userActions from '../../../../store/actions/userActions';
import * as systemActions from '../../../../store/actions/systemActions';

const GearList = (props) => {

        const [gearData, setGearData] = useState(props.userReducer.user_data.user_gear.data);
        const [loading, setLoading] = useState(true);
        const [refreshing, setRefreshing] = React.useState(false);


        useEffect(() => {
            console.log(gearData);
            console.log(props)

            if (props.userReducer.user_data.user_gear.is_fetched === false && props.userReducer.user_data.user_gear.fetching === false) {
                fetchGear();
            } else {
                setLoading(false);
            }

        }, []);


        const fetchGear = () => {
            console.log('pressed');
            props.dispatch(userActions.fetchUserGear());

        };

        const onRefresh = React.useCallback(() => {
            console.log('rerfeshing');
            props.dispatch(userActions.fetchUserGear());
        }, []);


        function getSportName(gearItem) {
            if (gearItem === 1) {
                return <Text>Windsurf</Text>;
            }
            if (gearItem === 3) {
                return <Text>Kite Surf</Text>;
            }
            return <Text>Oeps</Text>;
        }

        function getGearType(gear) {

            return <Text>Oeps</Text>;
        }

        function addGear(sportId, gearItem) {
            console.log(sportId)
            console.log(gearItem)
        }

        return (
            <ScrollView style={styles.outerBody}
                        refreshControl={
                            <RefreshControl
                                refreshing={refreshing}
                                onRefresh={onRefresh}
                            />
                        }
            >
                {loading ?
                    <View style={styles.activityIndicator}>
                        <ActivityIndicator animating={true} color={Colors.red800} size='large'/>
                    </View>
                    :
                    <View>
                        <View style={styles.bodyContent}>
                            {props.userReducer.user_data.user_gear.data.map((sportItem, index) => {
                                return (
                                    <List.Section title={getSportName(index)} titleStyle={{fontSize: 15, padding: 0}}>
                                        {Object.keys(sportItem).map((gearItem) => {
                                            return (
                                                <List.Accordion
                                                    title={gearItem}
                                                    left={props => <List.Icon {...props} icon="folder"/>}>
                                                    {Object.keys(sportItem[gearItem]).map((index) => {
                                                        let gear = sportItem[gearItem][index];

                                                        console.log(gear)
                                                        return (
                                                            <View>
                                                                <Divider style={styles.divider}/>
                                                                <View style={styles.itemRow}>

                                                                    { gear.userDefault === true &&
                                                                        <IconButton
                                                                            style={styles.defaultGearIcon}
                                                                            icon="star"
                                                                            color={Colors.yellow800}
                                                                            size={20}
                                                                            onPress={() => console.log('Pressed')}
                                                                        />
                                                                    }
                                                                    <Text style={{fontSize: 15}}>
                                                                        {gear.brand}
                                                                    </Text>
                                                                    <Text style={{fontSize: 15}}>
                                                                        {gear.typeValue}
                                                                    </Text>
                                                                    <Text style={{fontSize: 15}}>
                                                                        {gear.value}
                                                                    </Text>
                                                                    <Text style={{fontSize: 15}}>
                                                                        <IconButton
                                                                            icon="delete"
                                                                            color={'red'}
                                                                            size={20}
                                                                        />
                                                                    </Text>
                                                                </View>
                                                            </View>
                                                        );
                                                    })
                                                    }

                                                    <View style={styles.buttonRow}>
                                                        <Button icon="plus" mode="outlined" onPress={()=>{
                                                            addGear(index,gearItem);
                                                        }}>
                                                            Add new
                                                        </Button>
                                                    </View>
                                                </List.Accordion>
                                            );

                                        })
                                        }


                                    </List.Section>
                                );
                            })

                            }
                        </View>
                    </View>
                }
            </ScrollView>
        );
    }
;

const styles = StyleSheet.create({
    defaultGearIcon:{
        top:-10,
        right: -10
    },
    itemRow: {
        flexDirection: 'row',
        justifyContent: 'space-around',
        height: 30,
    },
    buttonRow: {

        flexDirection: 'row',
        justifyContent: 'center',
        marginTop: 15,
        marginLeft: -60,
    },
    sportHeader: {
        marginTop: 15,
        marginBottom: -20,
        marginLeft: 25,
    },
    divider: {
        backgroundColor: 'black',
        color: 'black',
    },
    textBox: {
        marginLeft: 8,
        marginTop: 10,
    },
    header: {
        backgroundColor: '#00BFFF',
    },
    tabs: {
        backgroundColor: '#fd0a0a',
        flexDirection: 'row',
        justifyContent: 'space-around',
        height: 50,
    },
    tabItem: {
        textAlign: 'center',
        justifyContent: 'center',
        backgroundColor: '#1558e7',
    },
    headerContent: {
        padding: 10,
        alignItems: 'center',
    },
    avatar: {
        width: 80,
        height: 80,
        borderRadius: 63,
        borderWidth: 4,
        borderColor: 'white',
        marginBottom: 10,
    },
    name: {
        fontSize: 22,
        color: '#FFFFFF',
        fontWeight: '600',
    },
    textInfo: {
        fontSize: 18,
        marginTop: 20,
        color: '#696969',
    },
    outerBody: {
        backgroundColor: 'white',
        flex: 1,
    },
    bodyContent: {
        flex: 1,
        padding: 15,
        textAlign: 'center',
    },
    activityIndicator: {
        flex: 1,
        marginTop: 150,
        justifyContent: 'center',
    },
    menuBox: {
        backgroundColor: '#DCDCDC',
        width: 100,
        height: 100,
        alignItems: 'center',
        justifyContent: 'center',
        margin: 12,
        shadowColor: 'black',
        shadowOpacity: .2,
        shadowOffset: {
            height: 2,
            width: -2,
        },
        elevation: 4,
    },
    icon: {
        width: 60,
        height: 60,
    },
    info: {
        fontSize: 22,
        color: '#696969',
    },
});


export default connect(store => {
    return {...store};
})(GearList);


