import {connect} from 'react-redux';
import {RefreshControl, SectionList, StyleSheet, Text, View} from 'react-native';
import React, {useEffect, useState} from 'react';
import {Button, Colors, Divider, Drawer, IconButton} from 'react-native-paper';
import * as userActions from '../../../../store/actions/userActions';
import {useNavigation} from '@react-navigation/native';
import {deleteUserGearById} from '../../../../store/actions/userActions';

const sportDetailList = (props) => {
    const navigation = useNavigation();

    const [refreshing, setRefreshing] = React.useState(false);
    const [showAddButton, setShowAddButton] = React.useState(false);
    const [sport, setSport] = useState(props.route.params.sport);
    const [userGear, setUserGear] = useState(props.userReducer.user_data.user_gear.data);

    const [listData, setListData] = useState([]);
    const [favoList, setFavoList] = useState([]);

    useEffect(() => {

        props.navigation.setOptions({
            title: sport.sportName +' gear',
        });

        sectionData();


    }, [props.navigation,props.userReducer.user_data.user_gear.data]);

    const sectionData = () =>{
        let data =  [];
        sport.sportGearType.map((gearTypeName) => {
            let dataItem = {title: gearTypeName, data: []}
            props.userReducer.user_data.user_gear.data.map((gearItem) => {
                if(sport.id === gearItem.sport.id && gearTypeName === gearItem.type){
                    dataItem.data.push(gearItem);
                    if(gearItem.user_default){
                        let arrayList = favoList;
                        let defaultGear = { type: gearItem.type, id:gearItem.id}
                        arrayList.push(defaultGear)
                        setFavoList(arrayList);
                    }
                }
            })
            data.push(dataItem)
            if(dataItem.data.length === 0){
                setShowAddButton(true)
            }
        })

        // console.log(data.)

        console.log('builded')

        setListData(data)
    }


    const onRefresh = React.useCallback(() => {
        props.dispatch(userActions.fetchUserGearNormaLized());
    });

    const updateUserDefault = (data) => {

        let arrayList = [];
        favoList.map((favoGear)=>{
            if(favoGear.type !== data.type){
                arrayList.push(favoGear);
            }
        })
        let newGearItem = { type: data.type, id:data.id};
        props.dispatch(userActions.updateFavogear(newGearItem));

        arrayList.push(newGearItem)
        setFavoList(arrayList)

        props.dispatch(userActions.fetchUserGear());
    }


    function removeGearItem(gear) {

        props.dispatch(userActions.deleteUserGearById(gear.id));

    }

    return (
        <View style={styles.container}>
            <SectionList
                refreshControl={
                    <RefreshControl
                        refreshing={refreshing}
                        onRefresh={onRefresh}
                    />
                }
                sections={listData}
                renderSectionHeader={({section}) => {
                        return (<Text style={styles.sectionHeader}>{section.title +'s'}</Text>)
                    }
                }
                ItemSeparatorComponent={(props) => <Divider style={styles.divider}/>}

                keyExtractor={(item, index) => index}
                renderItem={(data) =>{

                    let isFavorit = false;
                    favoList.map((favoGear)=>{
                        if(favoGear.type === data.item.type && favoGear.id === data.item.id){
                            isFavorit = true;
                        }
                    })

                    return (
                            <View style={styles.itemRow}>

                                <IconButton
                                    style={styles.defaultGearIcon}
                                    icon={isFavorit ? 'star' : 'star-outline'}
                                    color={Colors.yellow700}
                                    size={20}
                                    onPress={() => {
                                        updateUserDefault(data.item); //usual call like vanilla javascript, but uses this operator
                                    }}
                                />

                                <Text style={{fontSize: 15}}>
                                    {data.item.brand}
                                </Text>
                                <Text style={{fontSize: 15}}>
                                    {data.item.type_value}
                                </Text>
                                <Text style={{fontSize: 15}}>
                                    {data.item.value}
                                </Text>
                                <Text style={{fontSize: 15}}>
                                    <IconButton
                                        icon="delete"
                                        color={'red'}
                                        size={20}
                                        onPress={() => {
                                            removeGearItem(data.item); //usual call like vanilla javascript, but uses this operator
                                        }}
                                    />
                                </Text>
                            </View>
                    )
                }
                }

            />

            {showAddButton &&

            <Button mode="outlined"
                    onPress={()=>{
                        navigation.navigate('New gear');
                    }}
            >New gear</Button>
            }

        </View>
    );




    // return (
    //     <View>
    //         <Drawer.Item
    //             style={{
    //                 backgroundColor: '#64ffda',
    //                 padding: 0,
    //                 margin: 0,
    //             }}
    //             icon="star"
    //             label="First Item"
    //         />
    //
    //
    //         <Divider style={styles.divider}/>
    //         <View style={styles.itemRow}>
    //
    //             <IconButton
    //                 style={styles.defaultGearIcon}
    //                 icon="star"
    //                 color={Colors.yellow800}
    //                 size={20}
    //                 onPress={() => console.log('Pressed')}
    //             />
    //             <Text style={{fontSize: 15}}>
    //                 goya
    //                 {/*{gear.brand}*/}
    //             </Text>
    //             <Text style={{fontSize: 15}}>
    //                 banzai
    //                 {/*{gear.typeValue}*/}
    //             </Text>
    //             <Text style={{fontSize: 15}}>
    //                 4.5
    //                 {/*{gear.value}*/}
    //             </Text>
    //             <Text style={{fontSize: 15}}>
    //                 <IconButton
    //                     icon="delete"
    //                     color={'red'}
    //                     size={20}
    //                 />
    //             </Text>
    //         </View>
    //     </View>
    //
    //
    // );
};

const styles = StyleSheet.create({
    container: {
    },
    sectionHeader: {
        textAlign: "center",
        padding:15,
        fontSize: 17,
        fontWeight: 'bold',
        backgroundColor: 'rgb(215,215,215)',
    },
    item: {
        padding: 10,
        fontSize: 18,
        height: 44,
    },

    defaultGearIcon:{
    },
    itemRow: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-around',
        height: 50,
    },
    buttonRow: {

        flexDirection: 'row',
        justifyContent: 'center',
        marginTop: 15,
        marginLeft: -60,
    },
    sportHeader: {
        marginTop: 15,
        marginBottom: -20,
        marginLeft: 25,
    },
    divider: {
        backgroundColor: 'black',
        color: 'black',
    },
    textBox: {
        marginLeft: 8,
        marginTop: 10,
    },
    header: {
        backgroundColor: '#00BFFF',
    },
    tabs: {
        backgroundColor: '#fd0a0a',
        flexDirection: 'row',
        justifyContent: 'space-around',
        height: 50,
    },
    tabItem: {
        textAlign: 'center',
        justifyContent: 'center',
        backgroundColor: '#1558e7',
    },
    headerContent: {
        padding: 10,
        alignItems: 'center',
    },
    avatar: {
        width: 80,
        height: 80,
        borderRadius: 63,
        borderWidth: 4,
        borderColor: 'white',
        marginBottom: 10,
    },
    name: {
        fontSize: 22,
        color: '#FFFFFF',
        fontWeight: '600',
    },
    textInfo: {
        fontSize: 18,
        marginTop: 20,
        color: '#696969',
    },
    outerBody: {
        backgroundColor: 'white',
        flex: 1,
    },
    bodyContent: {
        flex: 1,
        padding: 15,
        textAlign: 'center',
    },
    activityIndicator: {
        flex: 1,
        marginTop: 150,
        justifyContent: 'center',
    },
    menuBox: {
        backgroundColor: '#DCDCDC',
        width: 100,
        height: 100,
        alignItems: 'center',
        justifyContent: 'center',
        margin: 12,
        shadowColor: 'black',
        shadowOpacity: .2,
        shadowOffset: {
            height: 2,
            width: -2,
        },
        elevation: 4,
    },
    icon: {
        width: 60,
        height: 60,
    },
    info: {
        fontSize: 22,
        color: '#696969',
    },
});
export default connect(store => {
    return {...store};
})(sportDetailList);


