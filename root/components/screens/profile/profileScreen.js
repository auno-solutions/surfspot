import {connect} from 'react-redux';
import React, {useEffect} from 'react';
import {TouchableOpacity} from 'react-native';
import {
    StyleSheet,
    Text,
    View,
    Image,
    ScrollView,
} from 'react-native';
import {Avatar, List, Divider, Subheading, Title, Button, IconButton} from 'react-native-paper';
// import GearList from './gear/gearList';
import GearList from './gear/gearList2';
import SettingsScreen from './settings/settinsScreen';
import ConnectionsScreen from './connections/connectionsScreen';
import {useNavigation} from '@react-navigation/native';

const ProfileScreen = (props) => {

        const [selectedTab, setSelectedTab] = React.useState(1);
        const navigation = useNavigation();
        // const [snackBar, setSnackBar] = React.useState(props.route.params.snackbar);


        function checkSnackbar() {
            console.log(props.route.params)
            // if (snackBar !== undefined) {
            //     props.navigation.setParams('snackbar', undefined);
            //     setSnackBar(undefined);
            //
            // }else{
            //     console.log('snack unbde')
            // }
        }

        useEffect(() => {
            // checkSnackbar();

        }, []);

        const changeTab = (tab) => {
            setSelectedTab(tab);
        };


        function showSelectedTab() {
            switch (selectedTab) {
                case 1:
                    return <GearList/>;
                // case 2:
                //     return <ConnectionsScreen/>;
                case 3:
                    return <SettingsScreen/>;
            }
        }


        return (
            <View style={styles.outerBody}>
                <View style={styles.header}>
                    <View style={styles.headerContent}>
                        <Image style={styles.avatar}
                               source={{uri: 'https://bootdey.com/img/Content/avatar/avatar1.png'}}/>
                        <Text style={styles.name}>
                            {props.authReducer.user.data.username}
                        </Text>
                    </View>
                </View>
                <View style={styles.tabs}>
                    <TouchableOpacity
                        style={styles.tabItem}
                        onPress={() => {
                            changeTab(1);
                        }}
                    >
                        <Title style={styles.tabHeader}>Gear</Title>
                        <Divider style={(selectedTab === 1 ? styles.tabDividerActive : styles.tabDivider)}/>
                    </TouchableOpacity>
                    {/*<TouchableOpacity*/}
                    {/*    style={styles.tabItem}*/}
                    {/*    onPress={()=>{*/}
                    {/*        changeTab(2)*/}
                    {/*    }}*/}
                    {/*>*/}
                    {/*    <Title style={styles.tabHeader}  >Connections</Title>*/}
                    {/*    <Divider style={(selectedTab === 2?styles.tabDividerActive:styles.tabDivider)}/>*/}
                    {/*</TouchableOpacity>*/}
                    <TouchableOpacity
                        style={styles.tabItem}
                        onPress={() => {
                            changeTab(3);
                        }}
                    >
                        <Title style={styles.tabHeader}>Settings</Title>
                        <Divider style={(selectedTab === 3 ? styles.tabDividerActive : styles.tabDivider)}/>
                    </TouchableOpacity>
                </View>
                {showSelectedTab()}
            </View>
        );
    }
;
const styles = StyleSheet.create({
    tabDividerActive: {
        backgroundColor: 'orange',
        marginLeft: 10,
        marginRight: 10,
        height: 2,
    },
    tabDivider: {
        marginLeft: 10,
        marginRight: 10,
        backgroundColor: 'black',
        height: 2,
    },
    itemRow: {
        flexDirection: 'row',
        justifyContent: 'space-around',
        height: 30,
    },
    buttonRow: {

        flexDirection: 'row',
        justifyContent: 'center',
        marginTop: 15,
        marginLeft: -60,
    },
    sportHeader: {
        marginTop: 15,
        marginBottom: -20,
        marginLeft: 25,
    },
    divider: {
        backgroundColor: 'black',
        color: 'black',
    },
    textBox: {
        marginLeft: 8,
        marginTop: 10,
    },
    header: {
        backgroundColor: '#00BFFF',
    },
    tabHeader: {
        textAlign: 'center',
    },
    tabs: {
        borderTopLeftRadius: 20,
        borderTopRightRadius: 20,
        backgroundColor: 'white',
        flexDirection: 'row',
        justifyContent: 'space-around',
        height: 50,
        marginTop: -20,
    },
    tabItem: {
        borderTopLeftRadius: 20,
        borderTopRightRadius: 20,
        flex: 1,
        justifyContent: 'space-evenly',
        textAlign: 'center',
        backgroundColor: 'white',
    },
    headerContent: {
        padding: 10,
        alignItems: 'center',
    },
    avatar: {
        width: 80,
        height: 80,
        borderRadius: 63,
        borderWidth: 4,
        borderColor: 'white',
        marginBottom: 0,
    },
    name: {
        marginBottom: 20,
        fontSize: 22,
        color: '#FFFFFF',
        fontWeight: '600',
    },
    textInfo: {
        fontSize: 18,
        marginTop: 20,
        color: '#696969',
    },
    outerBody: {
        backgroundColor: 'white',
        flex: 1,
    },
    bodyContent: {
        flex: 1,
        padding: 15,
        textAlign: 'center',
    },

    menuBox: {
        backgroundColor: '#DCDCDC',
        width: 100,
        height: 100,
        alignItems: 'center',
        justifyContent: 'center',
        margin: 12,
        shadowColor: 'black',
        shadowOpacity: .2,
        shadowOffset: {
            height: 2,
            width: -2,
        },
        elevation: 4,
    },
    icon: {
        width: 60,
        height: 60,
    },
    info: {
        fontSize: 22,
        color: '#696969',
    },
});


export default connect(store => {
    return {...store};
})(ProfileScreen);


