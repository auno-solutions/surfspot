import {connect} from 'react-redux';
import {StyleSheet, Text, View, ScrollView, ActivityIndicator,RefreshControl} from 'react-native';
import React, {useEffect} from 'react';

import {Card, Title, Paragraph} from 'react-native-paper';

import SessionsGraph from '../sessions/sessionsGraph';
import SportGraph from '../sessions/sportGraph';
import LinearGradient from 'react-native-linear-gradient';
import * as systemActions from '../../../store/actions/systemActions';
import * as userActions from '../../../store/actions/userActions';


const HomeScreen = (props) => {

    const [refreshing, setRefreshing] = React.useState(false);


    useEffect(() => {
        // props.dispatch({
        //     type: 'DEV_LOGOUT',
        //     payload: 'test'
        // });

        if (!props.userReducer.user_data.user_graphs.sessions_count.is_fetched) {
            props.dispatch(systemActions.fethUserSessionCount());
        }

    });


    const onRefresh = React.useCallback(() => {
        props.dispatch(systemActions.fethUserSessionCount());
        props.dispatch(systemActions.fethUserSessionsGraph());
        props.dispatch(systemActions.fethUserSportGraph());
        props.dispatch(userActions.fetchUserGear());
    }, []);

    return (
        <ScrollView style={styles.container}
                    refreshControl={
                        <RefreshControl
                            refreshing={refreshing}
                            onRefresh={onRefresh}
                        />
                    }
        >
            {/*#F9968B #F27348 #26474E #76CDCD #2CCED2*/}
            {/*#F4C815 #F9CAD7 #A57283 #C1D5DE #DEEDE6*/}
            <View style={styles.boxTop}>

                <Card style={styles.cardExtra}>
                    <Card.Content style={styles.cardContent}>
                        <Title style={styles.textColor}>My Friends</Title>
                        <Title style={styles.textColor}>0</Title>
                    </Card.Content>
                </Card>

                <Card style={styles.cardExtra}>
                    {props.userReducer.user_data.user_graphs.sessions_count.is_fetched === true ?
                        <Card.Content style={styles.cardContent}>
                            <Title style={styles.textColor}>My Sessions</Title>
                            <Title style={styles.textColor}>{props.userReducer.user_data.user_graphs.sessions_count.data.count}</Title>
                        </Card.Content>
                        :
                        <Card.Content style={styles.cardContent}>
                            <ActivityIndicator size="large" color="#00ff00"/>
                            <Text>Loading</Text>
                        </Card.Content>
                    }
                </Card>

            </View>
            <Card style={styles.chartBox}>
                <SportGraph/>
            </Card>
            <Card style={styles.chartBox}>
                <SessionsGraph/>
            </Card>
        </ScrollView>
    );
};

const styles = StyleSheet.create({
    textColor: {
        color: 'white',
    },
    linearGradient: {
        borderRadius: 20,
    },
    container: {
        backgroundColor: '#96e5ff',
        flex: 1,
    },
    cardExtra: {
        color: '#FFFFFF',
        backgroundColor: '#6930C3',
        borderRadius: 20,
    },

    boxTop: {
        margin: 20,
        flexDirection: 'row',
        justifyContent: 'space-evenly',
    },
    box: {
        flexWrap: 'wrap',
        margin: 20,
        flexDirection: 'row',
        justifyContent: 'space-evenly',
    },
    chartBox: {
        margin: 20,
        backgroundColor: '#4EA8DE',
        borderRadius: 20,
    },
    cardContent: {
        color: '#FFFFFF',
        justifyContent: 'center',
        alignItems: 'center',

    },
    list: {
        flex: 1,
    },
});


export default connect(store => {
    return {...store};
})(HomeScreen);


